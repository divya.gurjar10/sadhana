@extends('layouts.app')
@section('title', 'Sadhana Renewal Center')
@section('content')
<section class="about-page">
         <div class="page-header" style="background: url(images/about-us.jpg);">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <h1>About Us</h1>
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="welcome-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-lg-8 order-2 order-lg-1">
                     <div class="welcome-content">
                        <header class="entry-header">
                           <h2 class="entry-title">Introduction </h2>
                        </header>
                        <!-- .entry-header -->
                        <div class="entry-content mt-5">
                           <p style="padding-right: 10px;">Sadhana Renewal Center, Monvila is a joint initiative of the Latin Archdiocese of Thiruvananthapuram and Claretian Missionaries of St. Thomas Province. This center was officially inaugurated by His Grace Rt. Rev. Soosa Pakiam on 25 August 2006 as a rehabilitation center for the mentally disabled and for the care of the drug addicts and the alcoholics. In 2004 the archdiocese of Thiruvananthapuram temporarily entrusted the Claretian Missionaries of St. Thomas Province the building and the compound at Monvila for initiating missionary activities, particularly for the care of the alcoholics and the drug addicts. Accordingly, under the initiative of Fr. Alexander Kureekattil, CMF the Province started monthly retreat programs for the alcoholics and the drug addicts. Later, Fr. Alexander Kureekattil was deeply moved by the sad plight of the many wandering people in and around the capital city of Thiruvananthapuram. He envisioned the urgent need to care those people wandering in the streets, market places, bus stations, railway stations etc. as they are left alone due to various mental and psychological imbalances. Fr. Kureekattil picked 25 such persons from the streets and gave them shelter in Sadhana Renewal Center. This was the beginning of a great missionary endeavor. From August 2006 onwards, the ministry of caring and rehabilitating the mentally challenged has become part of the mainstream activity of Sadhana Renewal Center. Currently there are 80 inmates in the center, aged between 18 and 75.</p>
                        </div>
                        <!-- .entry-content -->
                     </div>
                     <!-- .welcome-content -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-4 order-1 order-lg-2">
                     <img src="images/welcome.jpg" alt="welcome" style="max-width: 100%;">
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="home-page-icon-boxes" style="padding-top: 0;">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-6 col-lg-6 mt-4 mt-lg-0">
                     <div class="icon-box">
                        <figure class="d-flex justify-content-center">
                           <img src="images/hands-gray.png" alt="">
                           <img src="images/hands-white.png" alt="">
                        </figure>
                        <header class="entry-header">
                           <h3 class="entry-title">Vision</h3>
                        </header>
                        <div class="entry-content">
                           <p>• An inclusive Society in which persons with mental disability will enjoy equal rights, opportunities and participation.
                              Sadhana envisions to contribute towards creating an inclusive and just society by integrating those who are side-lined or entirely ostracised due to their subnormal mental efficiencies and thereby ensuring them equal rights, opportunities and participation in social life
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-6 mt-4 mt-lg-0">
                     <div class="icon-box">
                        <figure class="d-flex justify-content-center">
                           <img src="images/donation-gray.png" alt="">
                           <img src="images/donation-white.png" alt="">
                        </figure>
                        <header class="entry-header">
                           <h3 class="entry-title">Mission</h3>
                        </header>
                        <div class="entry-content">
                           <p>• To create a conducive atmosphere for the holistic development of persons with mental disability and mainstreaming them with the society.
                              The mission of Sadhana is to provide a calm and conducive atmosphere for the convalescence and the holistic rehabilitation of mentally incompetent persons with a view to gradually mainstreaming them with the rest of the society.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="featured-cause abt-goal" style="margin-bottom: 50px;">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-lg-12">
                     <div class="cause-wrap d-flex flex-wrap justify-content-between" style="margin:0 0 50px 0;">
                        <figure class="m-0">
                           <img src="images/goal.png" alt="">
                        </figure>
                        <div class="cause-content-wrap">
                           <header class="entry-header d-flex flex-wrap align-items-center">
                              <h3 class="entry-title w-100 m-0"><a href="#"> Goals</a></h3>
                           </header>
                           <!-- .entry-header -->
                           <ul class="sadhana">
                              <li>To bring about attitudinal changes in the person with mental disabilities, in the family members and the society. </li>
                              <li>To build the capacity of the persons with mental disabilities for enhancing their competence and making them self-reliant.</li>
                              <li>To Promote and facilitate the rehabilitation of persons with mental disability.</li>
                           </ul>
                           <div class="entry-content">
                              <p class="m-0">Focussing on procedures, activities and therapies capable of bringing about the desired psycho-physical and social integration, the ministry of Sadhana seeks to make attitudinal changes in those under our care and in their family and strives to empower them in every way possible to be self-reliant and to facilitate rehabilitation into the normal family atmosphere. Apart from this, Sadhana also strives to eliminate the social taboo regarding psychopathology and to foster empathy and social acceptance by conscientizing various social groups. </p>
                           </div>
                           <!-- .entry-content -->
                        </div>
                        <!-- .cause-content-wrap -->
                     </div>
                     <!-- .cause-wrap -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-12">
                     <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                           <img src="images/core.png" alt="">
                        </figure>
                        <div class="cause-content-wrap">
                           <header class="entry-header d-flex flex-wrap align-items-center">
                              <h3 class="entry-title w-100 m-0"><a href="#"> Core Values </a></h3>
                           </header>
                           <!-- .entry-header -->
                           <ul class="sadhana">
                              <li>Respecting the dignity of all without discrimination</li>
                              <li>Respecting the rights of all </li>
                              <li>Inclusive love </li>
                              <li>Positive thinking </li>
                           </ul>
                           <div class="entry-content">
                              <p class="m-0">Defending the dignity of all humans irrespective of incapacities, upholding the human rights, inclusive and unconditional love, fostering positive thinking are the core values of Sadhana. </p>
                           </div>
                           <!-- .entry-content -->
                        </div>
                        <!-- .cause-content-wrap -->
                     </div>
                     <!-- .cause-wrap -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-12">
                     <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                           <img src="images/strategy.png" alt="">
                        </figure>
                        <div class="cause-content-wrap">
                           <header class="entry-header d-flex flex-wrap align-items-center">
                              <h3 class="entry-title w-100 m-0"><a href="#"> Strategies </a></h3>
                           </header>
                           <!-- .entry-header -->
                           <ul class="sadhana">
                              <li>Collaboration and networking with the government and other like-minded organizations. </li>
                              <li>Enhancing people’s participation through Community Based Organizations </li>
                              <li>Building the capacity of Persons with mental disability to assert their rights and access the government schemes.</li>
                           </ul>
                           <div class="entry-content">
                              <p class="m-0">To effectively reach the desired ends, Sadhana collaborates with the governmental as well as non-governmental organizations for the provisional and medical support, financial aid and for rehabilitative needs. From the very outset, Sadhana ensures a participative approach by involving the local community-based organizations. To avail those restricted lives, the governmental welfare schemes they deserve is another priority.</p>
                           </div>
                           <!-- .entry-content -->
                        </div>
                        <!-- .cause-content-wrap -->
                     </div>
                     <!-- .cause-wrap -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-12">
                     <div class="cause-wrap d-flex flex-wrap justify-content-between">
                        <figure class="m-0">
                           <img src="images/patner.png" alt="">
                        </figure>
                        <div class="cause-content-wrap">
                           <header class="entry-header d-flex flex-wrap align-items-center">
                              <h3 class="entry-title w-100 m-0"><a href="#"> Partnership with NGOs and Government Departments:  </a></h3>
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                              <p class="m-0">Sadhana Renewal Centre believes in the linkage and networking with other government and non-government agencies for the welfare of the persons with disabilities. We collaborate with different agencies like Ministry of Social Justice (Government of Kerala) state disability commission, Mental Health Center Trivandrum, General Hospital Trivandrum, Medical College Hospital, Trivandrum, Kerala Police, Loyola College Trivandrum, Marian College of Engineering Trivandrum, Marygiri College of Social Science Marthandam, Tamil Nadu etc. </p>
                           </div>
                           <!-- .entry-content -->
                        </div>
                        <!-- .cause-content-wrap -->
                     </div>
                     <!-- .cause-wrap -->
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="help-us">
            <div class="container">
               <div class="row">
                  <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                     <h2>Help us so we can help others</h2>
                     <a class="btn orange-border" href="{{url('donate/price-card')}}">Donate now</a>
                  </div>
               </div>
            </div>
         </div>
      </section>

@endsection
