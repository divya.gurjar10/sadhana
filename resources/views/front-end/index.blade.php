@extends('layouts.app')
@section('title', 'Sadhana Renewal Center')
@section('content')
<section>
         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
               <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
               <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
               <div class="carousel-item active" style="background: url(images/hero1.jpg);">
                  <div class="container">
                     <div class="banner-title">

                        <p>  “To labor and to suffer for the one we love is the greatest proof of our love.” <br><span style="font-size: 14px;"> -St.Antony Mary Claret</span></p>
                        <div class="d-flex flex-wrap align-items-center mt-5 banner-btn">
                           <a href="price-card.html" class="btn gradient-bg mr-2" style="background: #dc3545;">Donate Now</a>

                        </div>
                     </div>
                  </div>
               </div>
               <div class="carousel-item" style="background: url(images/hero2.jpg);">
                  <div class="container">
                     <div class="banner-title">
                        <p> “I see somebody dying, I pick him up. I find somebody hungry, I give him food. He can love and be loved. I don’t look at his color, I don’t look at his religion. I don’t look at anything. Every person whether he is Hindu, Muslim or Buddhist, he is my brother, my sister,”.<br> <span style="font-size: 14px;"> -St.Mother Theresa</span></p>
                        <div class="d-flex flex-wrap align-items-center mt-5 banner-btn">
                           <a href="price-card.html" class="btn gradient-bg mr-2" style="background: #dc3545;">Donate Now</a>

                        </div>
                     </div>
                  </div>
               </div>
               <div class="carousel-item" style="background: url(images/hero3.jpg);">
                  <div class="container">
                     <div class="banner-title">
                        <p> “For I was hungry, and you gave Me something to eat; I was thirsty, and you gave Me something to drink; I was a stranger, and you invited Me in”.<br> <span style="font-size: 14px;"> - Mathew 25:35</span></p>
                        <div class="d-flex flex-wrap align-items-center mt-5 banner-btn">
                           <a href="price-card.html" class="btn gradient-bg mr-2" style="background: #dc3545;">Donate Now</a>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
         </div>
      </section>
      <section>
         <div class="home-page-icon-boxes">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                     <div class="icon-box active">
                        <figure class="d-flex justify-content-center">
                           <img src="images/hands-gray.png" alt="">
                           <img src="images/hands-white.png" alt="">
                        </figure>
                        <header class="entry-header">
                           <h3 class="entry-title">Achievements</h3>
                        </header>
                        <div class="entry-content">
                           <p>Sadhana extends hands to help the people who are affected by HIV/AIDS. 55 such peoples are helped in different ways. </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                     <div class="icon-box">
                        <figure class="d-flex justify-content-center">
                           <img src="images/donation-gray.png" alt="">
                           <img src="images/donation-white.png" alt="">
                        </figure>
                        <header class="entry-header">
                           <h3 class="entry-title">Donate</h3>
                        </header>
                        <div class="entry-content">
                           <p>Currently housing 81 persons including the residents and the staff, the centre runs purely with the support and help of many good-willed people in and around Trivandrum especially the people from the coastal area. </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                     <div class="icon-box">
                        <figure class="d-flex justify-content-center">
                           <img src="images/worker.png" alt="">
                           <img src="images/worker-white.png" alt="">
                        </figure>
                        <header class="entry-header">
                           <h3 class="entry-title">What we do</h3>
                        </header>
                        <div class="entry-content">
                           <p>Rehabilitation of Mentally ill, whom we adopt
                           care of living with HIV/ ADIS
                           Evangelization /Preaching/Counselling
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="home-page-welcome">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-lg-6 order-2 order-lg-1">
                     <div class="welcome-content">
                        <header class="entry-header">
                           <h2 class="entry-title">About SADHANA</h2>
                        </header>
                        <!-- .entry-header -->
                        <div class="entry-content mt-5">
                           <p>Sadhana Renewal Center, Monvila is a joint initiative of the Latin Archdiocese of Thiruvananthapuram and Claretian Missionaries of St. Thomas Province. This center was officially inaugurated by His Grace Rt. Rev. Soosa Pakiam on 25 August 2006 as a rehabilitation center for the mentally disabled and for the care of the drug addicts and the alcoholics. In 2004 the archdiocese...</p>
                           <a href="#" class="btn btn-primary cause-btn" style="margin-top: 50px;">More</a>
                        </div>
                        <!-- .entry-content -->

                        <!-- .entry-footer -->
                     </div>
                     <!-- .welcome-content -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-6 mt-4 order-1 order-lg-2">
                     <img src="images/ab-sadhana.jpg" alt="welcome">
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section class="our-causes">
         <div class="container">
            <div class="section-heading">
               <h2 class="entry-title">How we run </h2>
            </div>
            <div class="owl-carousel carousel-main cause-content-wrap">
               <div class="card">
                  <img class="card-img-top" src="images/fdcl.jpg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title">Food Collection</h5>
                     <p class="card-text">Some quick example text to build on the Cause and make up the bulk of the card's content.</p>
                     <a href="#" class="btn btn-primary cause-btn">Donate</a>
                  </div>
               </div>
               <div class="card" >
                  <img class="card-img-top" src="images/dnt.jpg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title">Donations</h5>
                     <p class="card-text">Some quick example text to build on the Cause and make up the bulk of the card's content.</p>
                     <a href="#" class="btn btn-primary cause-btn">Donate</a>
                  </div>
               </div>
               <div class="card" >
                  <img class="card-img-top" src="images/tolit.jpg" alt="Card image cap">
                  <div class="card-body">
                     <h5 class="card-title">From Students (Toiletry items) </h5>
                     <p class="card-text">Some quick example text to build on the Cause and make up the bulk of the card's content.</p>
                     <a href="#" class="btn btn-primary cause-btn">Donate</a>
                  </div>
               </div>

            </div>
         </div>
      </section>
      <section>
         <div class="home-page-limestone">
            <div class="container">
               <div class="row align-items-end">
                  <div class="coL-12 col-lg-8">
                     <div class="section-heading">
                        <h2 class="entry-title">Achievements</h2>
                        <p class="mt-5" style="margin-bottom: 25px;">So far, we have been successful in the following:</p>
                      <ul class="sadhana" style="margin-bottom: 25px;">
                              <li class="d-flex align-items-center"> <span style="font-size: 25px; background: #dc3545;  color:#fff; padding:10px; margin-right: 15px;"> 31</span> Individuals have been reintegrated to their own families after the care and treatment in our centre</li>
                              <li class="d-flex align-items-center"><span style="font-size: 25px; background: #dc3545; padding: 10px;  color:#fff; margin-right: 15px;">10</span> Individuals who had no place of their own have been shifted to other centres after their treatment.</li>
                           </ul>
                      <a href="#" class="btn btn-primary cause-btn">More</a>
                     </div>
                     <!-- .section-heading -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-4">
                  <a class="twitter-timeline" data-width="300" data-height="400" href="https://twitter.com/TwitterDev">Tweets by TwitterDev</a>
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
@endsection
