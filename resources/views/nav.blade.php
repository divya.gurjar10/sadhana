

<header>
         <div class="top-header-bar">
            <div class="container">
               <div class="row flex-wrap justify-content-center justify-content-lg-between align-items-lg-center">
                  <div class="col-12 col-lg-8 d-none d-md-flex flex-wrap justify-content-center justify-content-lg-start mb-3 mb-lg-0">
                     <div class="header-bar-email">
                        MAIL: <a href="#">info@sadhana.com</a>
                     </div>
                     <!-- .header-bar-email -->
                     <div class="header-bar-text">
                        <p>PHONE: <span>0471 259 6874</span></p>
                     </div>
                     <!-- .header-bar-text -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-lg-4 d-flex flex-wrap justify-content-center justify-content-lg-end align-items-center">
                     <div class="cal d-flex">
                        <p><a href="retreet-calendar.html"><i class="fas fa-calendar-alt"></i><span style="margin-left: 5px;">Retreat Calendar</span></a> </p>
                        <p> <a href="mass-time.html"><i class="fas fa-praying-hands"></i><span style="margin-left: 5px;">Mass Timings</span></a></p>
                     </div>
                     <!-- .donate-btn -->
                  </div>
                  <!-- .col -->
               </div>
            </div>
         </div>
         <div class="nav-bar">
            <div class="container">
               <div class="row">
                  <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                     <div class="site-branding d-flex align-items-center">
                        <a class="d-block" href="index.html" rel="home"><img class="d-block" src="{{asset('images/logo.png')}}" alt="logo"></a>
                     </div>
                     <!-- .site-branding -->
                      <div class="hamburger-menu d-lg-none">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                     </div>
                     <nav class="site-navigation d-flex justify-content-end align-items-right">
                        <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-content-center">
                           <li class="current-menu-item"><a href="index.html">Home</a></li>
                           <li class="abt-nav"><a>About us</a>
                              <ul class="abt-sub">
                              	  <li><a href="{{url('/about')}}">About Sadhana</a></li>
                              	  <li><a href="who-we-are.html">Who we are</a></li>
                                  <li><a href="what-we-do.html">What we do</a></li>
                                  <li><a href="team.html">Our Team</a></li>

                                </ul>
                           </li>
                           <li><a href="gallery.html">Gallery</a></li>
                           <li><a href="events.html">Events</a></li>
                           <li><a href="achievements.html">Achievements</a></li>
                           <li><a href="contact.html">Contact</a></li>
                        </ul>
                        <li class="donate"><a href="{{url('donate/price-card')}}" class="btn orange-border">Donate</a></li>
                     </nav>
                     <!-- .site-navigation -->

                     <!-- .hamburger-menu -->
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </header>
