@extends('layouts.app')
@section('title', 'Sadhana Renewal Center')
@section('content')
<section class="about-page">
         <div class="page-header no-after" style="background: url(../images/dn-banner.jpg);">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <h1>Donations</h1>
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section> 
         <div class="contact-page-wrap">
         	<div class="container">
         	<ul class="breadcrumb">
                              <li><a href="javascript:void(0)">Select a price card</a></li>
                              <li></li>
                           </ul></div>
            <div class="container">
               <div class="row d-block">
                  <div class="donation-block d-flex">
                     <div class="col-md-12">
                           <h2>	Donation Price cards</h2>
                           <p>   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum impedit necessitatibus, reiciendis id in reprehenderit architecto, sequi deserunt aspernatur. Atque facilis eos consectetur tempora veritatis officia et illo, minima quos.</p>
                     </div>
                  </div>
               </div>
               <div class="row d-block">
                  <div class="donation-block d-flex">
                     <div class="col-md-9">
                        <div class="dn-blcok">
                             <div class="donation-range d-flex flex-wrap">
                              <div class="block active" id="1">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 1</span> - <span> ₹ 500</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[0]}}</h3>
                              </div>
                              <div class="block" id="2">
                                 <h4 class="blockm">  Range</h>
                                 <p class="blockm" id="501">  <span>₹ 501</span> - <span>₹ 1000</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[1]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 1001</span>-<span>₹ 1500</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[2]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 1501</span>-<span>₹ 2000</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[3]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 2001</span>-<span>₹ 2500</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[4]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 2501</span>-<span>₹ 3000</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[5]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 3001</span> -<span>₹ 3500</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[6]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p>  <span>₹ 3501</span> - <span>₹ 4000</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[7]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p> <span>₹ 4001</span> - <span>₹ 4500</span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[8]}}</h3>
                              </div>
                              <div class="block">
                                 <h4> Range</h4>
                                 <p> <span>₹ 4501</span> - <span>₹ 5000 </span></p>
                                 <h3 class="vacant-card-count">Vacant Price Cards : {{$count[9]}}</h3>
                              </div>
                     </div>
                  </div>
               </div>
                     <div class="col-md-3">
                        <div class="sl-range" style="margin-top: 23px;">
                            <h4>Selected Range</h4>
                            <p>	₹1 - ₹ 500</p>
                            <span id="vacant-card-no">	Vacant Price Cards : {{$count[0]}}</span>
                            <div class="sl-p"> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias excepturi, corrupti. Vel expedita tempore, molestias inventore dignissimos totam similique voluptas sequi? Nobis distinctio ipsam, minus ad veritatis tenetur quia cupiditate. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, dolorum similique. Similique tempora dolores ullam error, laborum excepturi sit. Quaerat harum cupiditate molestiae soluta laborum repudiandae libero magnam deserunt sequi.
                            </div>
                            <form action="{{route('select-card')}}" method="POST">
                              @csrf<!-- {{csrf_field()}} -->
                              <input id="range-value-from" type="hidden" name="range-value-from" value="1"/>
                              <input id="range-value-to" type="hidden" name="range-value-to" value="500"/>
                                <input id="active" type="hidden" name="active" value=""/>
                              <button type="submit" class="btn btn-primary">Continue</button>
                              @csrf
                            </form>
                            <!-- <a href="/donate/select-card" class="continue-button cn-btn d-flex justify-content-between align-items-center">
                              Continue <i class="fas fa-long-arrow-alt-right"></i>
                              <button type="button" onclick="passData()">click here for ajax Call</button>
                             </a> -->
                          </div>
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="help-us">
            <div class="container">
               <div class="row">
                  <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                     <h2>Help us so we can help others</h2>
                     <a class="btn orange-border" href="{{url('donate/price-card')}}">Donate now</a>
                  </div>
               </div>
            </div>
         </div>
      </section>


@endsection
@section('pagespecificscripts')
<script src="{{asset('js/choose-range.js')}}"></script>
@stop
