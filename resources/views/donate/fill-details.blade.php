@extends('layouts.app')
@section('title', 'Fill your Details')
@section('content')

<section class="about-page">
         <div class="page-header no-after" style="background: url(../images/dn-banner.jpg);">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <h1>Donations</h1>
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="contact-page-wrap">
            <div class="container">
               <ul class="breadcrumb">
                               <li><a href="javascript:void(0)">Select a price card</a></li>
                               <li><a href="javascript:void(0)"><span style="padding-left: 65px;">Select a vacant card</span></a></li>
                              <li><a href="javascript:void(0)"><span style="padding-left: 65px;" >Fill Your Details</span></a></li>
                              <li></li>
                           </ul>
            </div>
            <div class="container">
               <div class="row d-block">
       <!--          @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
      @endif -->
                 <form action="{{route('payment')}}" >
                   @csrf
                  <div class="donation-block d-flex">
                     <div class="col-md-9">
                        <div class="dn-blcok">
                           <h2>Fill Your Cards</h2>
                           <p>		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum impedit necessitatibus, reiciendis id in reprehenderit architecto, sequi deserunt aspernatur. Atque facilis eos consectetur tempora veritatis officia et illo, minima quos.</p>

                           <div class="vacant d-flex flex-wrap">
                              <div class="form d-flex flex-wrap">
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" id="donor-name" name="donor-name" placeholder="Name"  required="required">
                                 </div>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" id="donor-phno" name="donor-phno" placeholder="Phone No" required="required">
                                 </div>
                                 <div class="col-md-12">
                                    <input type="email" class="form-control" id="donor-email" name="donor-email" placeholder="Email id" required="required" >
                                 </div>
                                 <div class="col-md-6">
                                    <select  class="form-control" id="donor-country" name="donor-country" required="required">
                                       <option>Select Country</option>
                                       @foreach($data['countryList'] as $country)
                                        <option value="{{ $country->geo_name }}">{{ $country->geo_name }}</option>
                                       @endforeach
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                  <select class="form-control" name="donor-state" id="donor-state" required="required">
                                    <option>Select State</option>
                                    @foreach($data['stateList'] as $state)
                                    <option value="{{$state->geo_name}}">{{$state->geo_name}}</option>
                                     @endforeach
                                  </select>
                                 </div>
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" id="donor-city" name="donor-city" placeholder="City" required="required" >
                                 </div>
                                
                                 <div class="col-md-6">
                                    <input type="text" class="form-control" id="dono-zipcode" name="donor-zipcode" placeholder="Pin / Zipcode" required="required" >
                                 </div>
                                 <div class="col-md-12">
                                    <input type="text" class="form-control" id="donor-address" name="donor-address" placeholder="Adress" required="required">
                                 </div>
                                 <input type="hidden" name="ip_address" id="ip_address" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
                               
                               </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="sl-range3">
                            <h4>Selected Amount</h4>
                            <div class="one">{{$data['amount']}}</div>
                            <input type="hidden" name="donor-amount" value="{{$data['amount']}}">
                            <div class="sl-p"> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias excepturi, corrupti. Vel expedita tempore, molestias inventore dignissimos totam similique voluptas sequi? Nobis distinctio ipsam, minus ad veritatis tenetur quia cupiditate.
                            </div>
                             <input type="submit"  class="btn btn-primary" value="Payment"><a href="#payment"></a> </button>
                            <input type="hidden" value="Hidden Element" name="hidden">                      
                            <!-- <a href="make-payment.html" class="cn-btn d-flex justify-content-between align-items-center"> Continue <i class="fas fa-long-arrow-alt-right"></i></a> -->

                         </div>
                     </div>
                  </div>
                  @csrf
                </form>
               </div>
            </div>
            <!-- .container -->
         </div>
      </section>
      <section>
         <div class="help-us">
            <div class="container">
               <div class="row">
                  <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                     <h2>Help us so we can help others</h2>
                     <a class="btn orange-border" href="{{url('donate/price-card')}}">Donate now</a>
                  </div>
               </div>
            </div>
         </div>
      </section>

      @endsection
       <form action="{{url('donate/payment')}}"  id="payment">
    <!-- Note that the amount is in paise = 50 INR -->
    <script
        src="https://checkout.razorpay.com/v1/checkout.js"
        data-key="rzp_test_7gW8sOMB0OsjJu"
  
        data-name="PHPExpertise.com"
        data-description="Test Txn with RazorPay"

     
      
        data-theme.color="#F37254"
    ></script>
    <input type="hidden" value="Hidden Element" name="hidden">
  <!--     @section('pagespecificscripts')
      <script src="{{asset('js/pay.js')}}"></script>
      @stop
      -->