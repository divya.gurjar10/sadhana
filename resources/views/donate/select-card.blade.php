@extends('layouts.app')
@section('title', 'Sadhana Renewal Center')
@section('content')
<section class="about-page">
    <div class="page-header no-after" style="background: url(../images/dn-banner.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h1>Donations</h1>
          </div>
              <!-- .col -->
        </div>
            <!-- .row -->
      </div>        <!-- .container -->
    </div>
</section>
    
  <section>
      <div class="contact-page-wrap">
        <div class="container">
          <ul class="breadcrumb">
            <li><a  href="javascript:void(0)">Select a price card</a></li>
            <li><a href="javascript:void(0)"><span style="padding-left: 65px;">Select a vacant card</span></a></li>
            <li></li>
          </ul>
          <div class="row d-block">
              <div class="donation-block d-flex">
                  <div class="col-md-12">
                      <h2>	Select a vacant card</h2>
                      <p>		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum impedit necessitatibus, reiciendis id in reprehenderit architecto, sequi deserunt aspernatur. Atque facilis eos consectetur tempora veritatis officia et illo, minima quos.</p>
                  </div>
              </div>
              <div class="row d-block">
                <div class="donation-block d-flex">
                  <div class="col-md-9">
                    <div class="dn-blcok">
                      <div class="slimScrollDiv"  style="position: relative; overflow: hidden; width: auto; height: 500px;">
                        <div id="span-block" class="vacant d-flex flex-wrap justify-content-center" style="overflow: scroll; width: auto; height: 500px;">                                  
                            @foreach ($cards as $card)
                     
                              @if ($card->active  == 1)
                                <div id="{{'r'.$card->active}}" class="block">{{"₹ ". $card->card_value}}</div>
                              @else
                                <div id="{{'r'.$card->card_value}}" class="block book">{{"₹ ". $card->card_value}}</div>
                              @endif
                            @endforeach
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="sl-range">
                      <h4>Selected Amount</h4>
                        <div class="one"  ></div>
                        <div class="sl-p"> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias excepturi, corrupti. Vel expedita tempore, molestias inventore dignissimos totam similique voluptas sequi? Nobis distinctio ipsam, minus ad veritatis tenetur quia cupiditate.
                        </div>
                        <!-- <a href="fill-details.html" class="cn-btn d-flex justify-content-between align-items-center"> Continue <i class="fas fa-long-arrow-alt-right"></i></a> -->

                            <form action="{{route('fill-details')}}" method="POST">
                            @csrf 
                            <!--  @csrf {{csrf_field()}}  -->
                            <!-- .not-allowed {cursor: not-allowed;} -->

                              <input id="amount-selected" type="hidden" name="amount-selected" value="" required="required" />
                              <button type="submit" class="btn btn-primary" id="select__card__continue" disabled="disabled" >Continue</button>
                              @csrf
                            </form>
                            <!-- <a href="fill-details.html" class="cn-btn d-flex justify-content-between align-items-center"> Continue <i class="fas fa-long-arrow-alt-right"></i></a> -->
                          </div>
                </div>  
              </div> 
             </div>
          </div>
            <!-- .container -->
        </div>
  </section>
      <section>
         <div class="help-us">
            <div class="container">
               <div class="row">
                  <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                     <h2>Help us so we can help others</h2>
                     <a class="btn orange-border" href="price-card.html">Donate now</a>
                  </div>
               </div>
            </div>
         </div>
      </section>

@endsection
@section('pagespecificscripts')
<script src="{{asset('js/select-card.js')}}"></script>
@stop
