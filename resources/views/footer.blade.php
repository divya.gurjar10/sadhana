<footer class="site-footer">
         <div class="footer-widgets">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-6 col-lg-5">
                     <div class="foot-about">
                        <h2><a class="foot-logo" href="#"><img src="images/foot-logo.png" alt=""></a></h2>
                        <p>Sadhana Renewal Center, Monvila is a joint initiative of the Latin Archdiocese of Thiruvananthapuram and Claretian Missionaries of St. Thomas Province. This center was officially inaugurated by His Grace Rt. Rev. Soosa Pakiam on 25 August 2006 as a rehabilitation center for the mentally disabled and for the care of the drug addicts and the alcoholics.</p>
                        <ul class="d-flex flex-wrap align-items-center">
                           <li><a href="https://www.facebook.com/sadhana.renewal.7"><i class="fab fa-facebook-f"></i></i></a></li>
                           <li><a href="https://twitter.com/rezepty"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                     </div>
                     <!-- .foot-about -->
                  </div>
                  <!-- .col -->
                  <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                     <h2>Useful Links</h2>
                     <ul>
                        <li><a href="who-we-are.html">Who we are</a></li>
                        <li><a href="what-we-do.html">What we do</a></li>
                        <li><a href="gallery.html">gallery</a></li>
                     </ul>
                  </div>
                  <!-- .col -->

                  <!-- .col -->
                  <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0">
                     <div class="foot-contact">
                        <h2>Contact</h2>
                        <ul>
                           <li><i class="fas fa-phone-volume"></i><span>0471 259 6874</span></li>
                           <li><i class="far fa-envelope"></i><span>info@sadhana.com</span></li>
                           <li><i class="fas fa-map-marker-alt"></i><span>Monvilla, Kullathoor, Thiruvananthapuram, Kerala 695011</span></li>
                        </ul>
                     </div>
                     <!-- .foot-contact -->
                     <div class="subscribe-form">
                        <form class="d-flex flex-wrap align-items-center">
                           <input type="email" placeholder="Your email">
                           <input type="submit" value="send">
                        </form>
                        <!-- .flex -->
                     </div>
                     <!-- .search-widget -->
                  </div>
                  <!-- .col -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
         <!-- .footer-widgets -->
         <div class="footer-bar">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <p class="m-0">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright ©<script>document.write(new Date().getFullYear());</script> All rights reserved | SADHANA
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                     </p>
                  </div>
                  <!-- .col-12 -->
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
         <!-- .footer-bar -->
      </footer>


     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.js"></script>"> 
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

      <!-- <script src="{{asset('js/script.js')}}"></script> -->
          
<script src="https://code.jquery.com/jquery-1.11.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<!-- <script src="{{asset('js/form-validation.js')}}"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->

  
      <script>
      $('#span-block').slimScroll({
      height: '500px'
      });
      </script>

      @yield('pagespecificscripts') 
 
<!-- <script>
 
      $(function () {
   $('#fillDonorDetails').validate({
      rules: {     
         donor-name: { 
            required: true,    
         },
         donor-phno: { 
            required: true,    
         },
         donor-email: { 
            required: true,    
         },
          donor-address: { 
            required: true,    
         },
          donor-city: { 
            required: true,    
         },
          donor-state: { 
            required: true,    
         },
         donor_country: { 
            required: true,    
         },
         donor-zipcode: { 
            required: true,    
         }, 
    },


    messages: {
         donor-name: { 
            required:"Please Enter Your Name",    
         },
          donor-phno: { 
            required: "Please Enter Phone number",    
         },
          donor-email: { 
            required: "Please Enter email",    
         },
           donor-address: { 
            required: "Please Enter Address",    
         },
           donor-city: { 
            required: "Please Enter City",    
         },
           donor-state: { 
            required: "Please Select State",    
         },
         donor-country: { 
             donor-required: "Please Select Country ",    
         },
           donor-zipcode: { 
            required: "Please Enter Pincode",    
         }
    },

      submitHandler: function(form) {
          form.submit();
       }
   });

   
</script> -->
  
 
 