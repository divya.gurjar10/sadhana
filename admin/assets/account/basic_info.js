function updatebasic() 
{
    $.ajax({
        url: site_url+"/admin/updateUserInfo",
        data: $('#updatebasicinfo').serialize(),
        type: 'POST',
        dataType: 'json',
        success: function(response)
        {
           toastr.success("Basic Info Updated");
        }

    });
}

function updateprofileimage()
{
    // alert($('#updateprofileimage').serialize());
    $.ajax({
        url: site_url+"/admin/userProfilePicture",
        data: $('#updateprofileimage').serialize(),
        type: 'POST',
        dataType: 'json',
        success: function(response)
        {
           toastr.success("profile Updated");
        }

    });
}


function changePassword()
{
    $.ajax({
        url: site_url+"/admin/updatePassword",
        data: $('#changePassword').serialize(),
        type: 'POST',
        dataType: 'json',
        success: function(response)
        {
           toastr.success("Password Updated");
        }
    });

}


$("form[name='changePassword']").validate({
    rules: {     
        password: { 
               required: true,    
            },
    },
    messages: {
         password: {
            required: "Please Enter password",
        }  
    },

    submitHandler: function (form) {
         changePassword();
        //form.submit();
       
    }
});

$("form[name='updateprofileimage']").validate({
    rules: {
        
        image: {
            required: true
        }
    },
    messages: {
        image: {
            required: "Please select an image"
        }
    },

    submitHandler: function (form) {
        // updateprofileimage();
        form.submit();
       
    }
});

$("form[name='updatebasicinfo']").validate({
    rules: {
        f_name: {
            required: true,
            regex: "^[a-zA-Z]*$",
        },
        l_name: {
            required: true,
            regex: "^[a-zA-Z]*$",
        },
        phone: {
            required: true,
            regex: "^[0-9]{10}",
        },
        name_of_organisation: {
            required: true,
            regex: "^[a-zA-Z]*$",
        },
        establish_year: {
            required: true,
            regex: "^[0-9]{4}",
        },
        intial_investment: {
            regex: "^[0-9.]*$",
        },
        registration_no: {
             required: true,
            regex: "^[a-zA-Z0-9]*$",
        },
        city: {
             required: true,
            regex: "^[a-zA-Z]*$",
        },
        state: {
             required: true,
            regex: "^[a-zA-Z ]*$",
        },
        address: {
             required: true,
            regex: "^[a-zA-Z0-9/, ]*$",
        },
        postal_code: {
             required: true,
            regex:  "^[0-9]{6}",
        },
        aadhar_no: {
             required: true,
            regex:  "^[0-9]{12}",
        }
    },


    messages: {
        f_name: {
            required: "Please Provide First Name",
            regex:"Only Alphabets Allowed "
        },
        l_name: {
            required: "Please Provide Last Name",
            regex:"Only Alphabets Allowed "
        },
        phone: {
            required: "Please Provide Phone",
            regex:"Only Numbers Allowed "
        },
        name_of_organisation: {
            required: "Please Provide Name Of Organisation",
            regex:"Only Alphabets Allowed "
        },
        establish_year: {
            required: "Please Provide Establish Year",
            regex:"Only Year Allowed "
        },
        intial_investment: {
            required: "Please Provide Intial Investment",
            regex:"Only Numbers Allowed "
        },
        registration_no: {
            required: "Please Provide Registration No.",
            regex:"No Special Character Allowed"
        },
        city: {
            required: "Please Provide City",
            regex:"Only Alphabets Allowed "
        },
        state: {
            required: "Please Provide State",
            regex:"Only Alphabets Allowed ",
        },
        address: {
            required: "Please Provide Address",
            regex:"No Special Character Except(/) Allowed"
        },
        postal_code: {
            required: "Please Provide Postal Code",
            regex:"Only 6 Digits Allowed "
        },
        aadhar_no: {
            required: "Please Provide Aadhar No.",
            regex:"Only 12 Digits Allowed "
        } 
    },

    submitHandler: function (form) {
        updatebasic();
       
    }
}); 

