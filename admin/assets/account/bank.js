$(function () {

    $("#bank_details_table").DataTable({

        "pageLength": 10,
        lengthMenu: [
        [ 10, 25, 50, 100, 250, 500 ],
        [ '10', '25', '50', '100', '250', '500']
        ],
        "processing":true,  
        "serverSide":true,  
        "ajax":{  
            url: site_url+"/bank/fetch_details",  
            type:"POST"  
        }, 
        "order": [[ 0, "asc" ]],
        "columnDefs": [ {
            "targets": 0,
            "orderable": false,
            "class": "white_space"
        }],
    });
});



$("form[name='updatebankdetail']").validate({
    rules: {
        account_no: {
            required: true
        },
        account_holder_name: {
            required: true
        },
        bank_name: {
            required: true
        },
        ifsc_code: {
            required: true
        },
        type: {
            required: true
        }
    },


    messages: {
        account_no: {
            required: "Please provide account number"
        },
        account_holder_name: {
            required: "Please provide account holder name"
        },
        bank_name: {
            required: "Please provide bank name"
        },
        ifsc_code: {
            required: "Please provide IFSC code"
        },
        type: {
            required: "Please provide type"
        }
    },

    submitHandler: function (form) {
        // updateprofileimage();
        form.submit();
       
    }
});






 function openBankModal(bank_id)
    {
        $.ajax({
            url: site_url+"/bank/getbBankDetail",
            type: "POST",
            data:{'bank_id':bank_id},
            dataType: "json",
            success: function(response) 
            {
                console.log(response.account_no);
                $('#update_bank_id').val(bank_id);
                $('#update_account_no').val(response.account_no);
                $('#update_account_holder_name').val(response.account_holder_name);
                $('#update_bank_name').val(response.bank_name);
                $('#update_ifsc_code').val(response.ifsc_code);
                $('#update_type').val(response.type);
                $('#openBankModal').modal('show');
            }   
        });
    }