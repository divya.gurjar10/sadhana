// Wait for the DOM to be ready
$(function() {

    var siteUrl = $('#site_url_path_for_external_js').val();
    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );



    // start for date
    jQuery.validator.addMethod("greaterThan", 
    function(value, element, params) {

        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) >= new Date($(params).val());
        }

        return isNaN(value) && isNaN($(params).val()) 
            || (Number(value) >= Number($(params).val())); 
    },'Must be greater than {0}.');
    //  end

$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {0}');

"use strict";

 jQuery.validator.addMethod("extension", function (value, element, param) {
                param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif|JPG|JPEG|PNG";
                return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
            }, 'Please enter a value with a valid extension.');
 
// $("#updateAdminProfileImage").validate({
//     rules: {
//         image: {
//             required: true,
//             extension: "jpg|jpeg|png|PNG|JPG|JPEG",
//             // filesize: 10,
//         }
//     },


//     messages: {
//         image: {
//             required: "Please select an image",
//             extension: "Please upload a valid file (jpg, jpeg, png)",
//             // filesize: "File size should be less than 10MB",
//         }
//     },

//     submitHandler: function (form) {
//         form.submit();
//     }
// });


$("#add_newsletter").validate({
    rules: {
        image: {
            extension: "jpg|jpeg|png|PNG|JPG|JPEG",
        },
        newsletter_title: {
            required: true
        },
        newsletter_content: {
            required: true
        },
    },


    messages: {
        image: {
            extension: "Please upload a valid file (jpg, jpeg, png)",
        },
        newsletter_title: {
            required: "Please provide title",
        },
        newsletter_content: {
            required: "Please select from content"
        },
    },

    submitHandler: function (form) {
        form.submit();
    }
});



$("#update_newsletter").validate({
    rules: {
        image: {
            extension: "jpg|jpeg|png|PNG|JPG|JPEG",
        },
        newsletter_title: {
            required: true
        }
    },


    messages: {
        image: {
            extension: "Please upload a valid file (jpg, jpeg, png)",
        },
        newsletter_title: {
            required: "Please provide title",
        }
    },

    submitHandler: function (form) {
        form.submit();
    }
});




$("#add_jobs_form").validate({
    rules: {
        job_title: {
            required: true
        },
        job_category: {
            required: true
        },
        experience_preferred: {
            required: true
        },
        job_description: {
            required: true
        },
        skills_needed: {
            required: true
        },
        salary_range_from: {
            regex: "^[0-9.]*$"
        },
        salary_range_to: {
            regex: "^[0-9.]*$"
        }
    },


    messages: {
        job_title: {
            required: "Please provide title"
        },
        job_category: {
            required: "Please select category"
        },
        experience_preferred: {
            required: "Please provide event name"
        },
        job_description: {
            required: "Please provide description"
        },
        skills_needed: {
            required: "Please provide skills"
        },
        salary_range_from: {
            regex: "Only number and . allowed"
        },
        salary_range_to: {
            regex: "Only number and . allowed"
        }
    },

    submitHandler: function (form) {
        // form.submit();
        addJobsFormSubmit();
    }
});



$("#update_jobs_form").validate({
    rules: {
        job_title: {
            required: true
        },
        job_category: {
            required: true
        },
        experience_preferred: {
            required: true
        },
        job_description: {
            required: true
        },
        skills_needed: {
            required: true
        },
        salary_range_from: {
            regex: "^[0-9.]*$"
        },
        salary_range_to: {
            regex: "^[0-9.]*$"
        }
    },


    messages: {
        job_title: {
            required: "Please provide title"
        },
        job_category: {
            required: "Please select category"
        },
        experience_preferred: {
            required: "Please provide event name"
        },
        job_description: {
            required: "Please provide description"
        },
        skills_needed: {
            required: "Please provide skills"
        },
        salary_range_from: {
            regex: "Only number and . allowed"
        },
        salary_range_to: {
            regex: "Only number and . allowed"
        }
    },

    submitHandler: function (form) {
        // form.submit();
        updateJobsFormSubmit();
    }
});
$("form[name='add_events_form']").validate({
    // Specify validation rules
    rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        event_name: {
            required: true
        },
        event_date_from: {
            required: true
        },
        event_date_to: {
            required: true,
            greaterThan: "#event_date_from"
        },
        event_venue: {
            required: true,
            regex: "^[a-zA-Z0-9 ]*$"
        },
        image: {
            extension: "jpg|jpeg|png|PNG|JPG|JPEG",
        }
    },
    messages: {
        event_name: {
            required: "Please provide event name",
        },
        event_date_from: {
            required: "Please select from date"
        },
        event_date_to: {
            required: "Please select to date",
            greaterThan: "To date should be greater than from date"
        },
        event_venue: {
            required: "Please provide the venu",
            regex: "Please provide alphabets and numbers only"
        },
        image: {
            extension: "Please upload a valid file (jpg, jpeg, png)",
        }
    },

    submitHandler: function (form) {

        form.submit();
    }
});
    

$("form[name='update_events_form']").validate({
    // Specify validation rules
    rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        event_name: {
            required: true
        },
        event_date_from: {
            required: true
        },
        event_date_to: {
            required: true,
            greaterThan: "#edit_event_date_from"
        },
        event_venue: {
            required: true,
            regex: "^[a-zA-Z0-9 ]*$"
        },
        image: {
            extension: "jpg|jpeg|png|PNG|JPG|JPEG",
        }
    },
    messages: {
        event_name: {
            required: "Please provide event name",
        },
        event_date_from: {
            required: "Please select from date"
        },
        event_date_to: {
            required: "Please select to date",
            greaterThan: "To date should be greater than from date"
        },
        event_venue: {
            required: "Please provide the venu",
            regex: "Please provide alphabets and numbers only"
        },
        image: {
            extension: "Please upload a valid file (jpg, jpeg, png)",
        }
    },

    submitHandler: function (form) {

        form.submit();
    }
});

    $("form[name='admin_change_password']").validate({
        // Specify validation rules
        rules: {
            password: {
                required: true,
                //regex: "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                regex: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d$@$!%*?&]{8,}"
            },
            cpassword: {
                equalTo: "#newpass"
            },
        },

        // Specify validation error messages
        messages: {
            password: {
                required: "Please Enter Password",
                regex: "Password must contain 8 characters with at least one lower case letter, one capital case letter and one digit"
            },
            cpassword: {
                equalTo: "Password & Confirm Password doesn't match"
            },
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("form[name='update_admin_basic_info_form']").validate({
        // Specify validation rules
        rules: {
            email: {
                required: true,
                email: true
            }
        },

        // Specify validation error messages
        messages: {
            email: {
                required: "Please enter an Email",
                email: "Please enter valid Email"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("form[name='create_user_form']").validate({
        // Specify validation rules
        rules: {
            f_name:{
                required: true
            },
            l_name:{
                required: true
            },
            mobile: {                
                required: true,
                regex: "^[0-9\-\()\'+']",
                rangelength: [7, 20]
            },
            address:{
                required: true
            },
            state:{
                required: true
            },
            city:{
                required: true
            },
            pincode:{
                required: true,
                rangelength: [6, 8]
            },
            username:{
                required: true,
            },
            password:{
                required: true,
            },
            password: {
                required: true,
                //regex: "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$"
                regex: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d$@$!%*?&]{8,}"
            },
            confirm_pass: {
                required: true,
                equalTo: "#newpass"
            },
        },

        // Specify validation error messages
        messages: {
            f_name: {
                required:"Please enter first name"
            },
            l_name: {
                required:"Please enter last name"
            },
            mobile: {
                required: "Please enter mobile number",
                regex: "Number must be contain dash(-), plus(+), dot(.) and brackets",
                rangelength: "Please provide number between 7 to 20 characters"
            },
            address:{
                required: "Please enter address"
            },
            state:{
                required: "Please select state"
            },
            city:{
                required: "Please enter city"
            },
            pincode:{
                required: "Please enter pincode",
                rangelength: "Please provide number between 6 to 7 characters"
            },
            username:{
                required: "Please enter username"
            },
            password: {
                required: "Please entre password",
                regex: "Password must contain 8 characters with at least one lower case letter, one capital case letter and one digit"
            },
            confirm_pass: {
                required: "Please entre confirm password",
                equalTo: "Password & Confirm Password doesn't match"
            },
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            createCustomerFormSubmit();
        }
    });

    $("form[name='update_user_form']").validate({
        // Specify validation rules
        rules: {
            f_name:{
                required: true
            },
            l_name:{
                required: true
            },
            mobile: {                
                required: true,
                regex: "^[0-9\-\()\'+']",
                rangelength: [7, 20]
            },
            address:{
                required: true
            },
            state:{
                required: true
            },
            city:{
                required: true
            },
            pincode:{
                required: true,
                rangelength: [6, 8]
            }
        },

        // Specify validation error messages
        messages: {
            f_name: {
                required:"Please enter first name"
            },
            l_name: {
                required:"Please enter last name"
            },
            mobile: {
                required: "Please enter mobile number",
                regex: "Number must be contain dash(-), plus(+), dot(.) and brackets",
                rangelength: "Please provide number between 7 to 20 characters"
            },
            address:{
                required: "Please enter address"
            },
            state:{
                required: "Please select state"
            },
            city:{
                required: "Please enter city"
            },
            pincode:{
                required: "Please enter pincode",
                rangelength: "Please provide number between 6 to 7 characters"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            updateCustomerFormSubmit();
        }
    });



    $("form[name='create_vehicle_form']").validate({
        // Specify validation rules
        rules: {
            vehicle_number:{
                required: true
            },
            driver_name:{
                required: true,
            },
            vehicle_type:{
                required: true,
            },
            driver_phone:{
                required: true,
                number: true,
            },
            note: {                
                required: true,
            }
        },

        // Specify validation error messages
        messages: {
            vehicle_number: {
                required:"Please enter vehicle number"
            },
            vehicle_type: {
                required: "Please select vehicle type"
            },
            driver_name: {
                required: "Please select vehicle type"
            },
            driver_phone: {
                required: "Please enter driver number",
                number: "Please enter valid number"
            },
            note: {
                required: "Please enter note"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            createVehicleFormSubmit();
        }
    });


    $("form[name='update_vehicle_form']").validate({
        // Specify validation rules
        rules: {
            vehicle_number: {
                required: true,
            },
            vehicle_type: {
                required: true,
            },
            driver_name: {
                required: true,
            },
            driver_phone: {
                required: true,
                // number: true,
            },
            note: {                
                required: true,
            }
        },

        // Specify validation error messages
        messages: {
            vehicle_number: {
                required:"Please enter vehicle number",
            },
            vehicle_type: {
                required: "Please select vehicle type",
            },
            driver_name: {
                required: "Please select vehicle type",
            },
            driver_phone: {
                required: "Please enter driver number",
                // number: "Please enter valid number",
            },
            note: {
                required: "Please enter note",
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            updateVehicleFormSubmit();
        }
    });


    $("form[name='add_earning_form']").validate({
        // Specify validation rules
        rules: {
            earning_vehicle_id:{
                required: true
            },
            earning_amount:{
                required: true,
                number: true,
            },
            earning_date:{
                required: true,
                date:true,
            },
            earning_note: {                
                required: true,
            }
        },

        // Specify validation error messages
        messages: {
            earning_vehicle_id: {
                required:"Please select vehicle number"
            },
            earning_amount: {
                required: "Please enter amount"
            },
            earning_date: {
                required: "Please select earning date"
            },
            earning_note: {
                required: "Please enter note"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            addEarningFormSubmit();
        }
    });


    $("form[name='update_earning_form']").validate({
        // Specify validation rules
        rules: {
            earning_vehicle_id:{
                required: true
            },
            earning_amount:{
                required: true,
                number: true,
            },
            earning_date:{
                required: true,
                date:true,
            },
            earning_note: {                
                required: true,
            }
        },

        // Specify validation error messages
        messages: {
            earning_vehicle_id: {
                required:"Please select vehicle number"
            },
            earning_amount: {
                required: "Please enter amount"
            },
            earning_date: {
                required: "Please select earning date"
            },
            earning_note: {
                required: "Please enter note"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            updateEarningFormSubmit();
        }
    });


    $("form[name='add_expense_form']").validate({
        // Specify validation rules
        rules: {
            earning_vehicle_id:{
                required: true
            },
            earning_amount:{
                required: true,
                number: true,
            },
            earning_date:{
                required: true,
                date:true,
            },
            earning_note: {                
                required: true,
            }
        },

        // Specify validation error messages
        messages: {
            earning_vehicle_id: {
                required:"Please select vehicle number"
            },
            earning_amount: {
                required: "Please enter amount"
            },
            earning_date: {
                required: "Please select expense date"
            },
            earning_note: {
                required: "Please enter note"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            addExpenseFormSubmit();
        }
    });


    $("form[name='update_expense_form']").validate({
        // Specify validation rules
        rules: {
            expense_vehicle_id:{
                required: true
            },
            expense_amount:{
                required: true,
                number: true,
            },
            expense_date:{
                required: true,
                date:true,
            },
            expense_note: {                
                required: true,
            }
        },

        // Specify validation error messages
        messages: {
            expense_vehicle_id: {
                required:"Please select vehicle number"
            },
            expense_amount: {
                required: "Please enter amount"
            },
            expense_date: {
                required: "Please select expense date"
            },
            expense_note: {
                required: "Please enter note"
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            updateExpenseFormSubmit();
        }
    });

    













$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});







});


