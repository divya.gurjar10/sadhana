<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Main Controller
 */
class Admin extends Main_Controller
{
    public $userInfo = null;
    public $login_party_id = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->model('adminmodel');
        $this->load->model('image_resize_model');
        $this->load->helper(array('url'));

        $this->userInfo = $this->session->admin_login_data;

        $this->admin_id = $this->userInfo['admin_id'];

        $this->checkAuthentication();
    }

    // check for authrnticate user
    public function checkAuthentication() 
    {
        $admin_id = $this->userInfo['admin_id'];
        if(!isset($admin_id) && empty($admin_id))  {
            redirect('login/loginPage');
        }
    }

    // Dashboard page
    
    public function index($active = "basic_info")
    {
        $this->mPagetitle = 'Dashboard';
        $this->mViewData['data'] = array(
            'pageName' => 'DASHBOARD',
        );
        $this->render('admin/dashboard');

    }

    // Account Page
    public function account($active = "basic_info")
    {
        $model_data = array(
            'admin_id' => $this->userInfo['admin_id'],
        );
        $rate=$this->adminmodel->getRate();
        $adminBasicInfo=$this->adminmodel->getUserBasicInfo($model_data['admin_id']);
        $passwordInfo=$this->adminmodel->getPasswordInfo();
       

        $this->mPagetitle = 'Account';
        $this->mViewData['data'] = array(
            'admin_id' => $this->userInfo['admin_id'],
            'e_mail' => $this->input->post('e_mail'),
            'password' => $this->input->post('password'),
            'pageName' => 'ACCOUNT',
            'active'=>$active,
            'adminBasicInfo'=>$adminBasicInfo,
            'passwordInfo'=>$passwordInfo,
            'rate'=>$rate
        );
        $this->render('admin/account'); 

    }

    //Donar Page

    public function donor($active = "donar_info")
    {
         $this->mPagetitle = 'Donar';
          $this->mViewData['data'] = array(
            'id' => $this->input->post('id'),
            'pageName' => 'DONOR',
            'active'=>$active,
            'donorList' => $this->adminmodel->getDonorList()    
        );
      
        $this->render('admin/donor');
    }


        // ============================== Update Controller Request ==================================

    // Update Admin Basic Info
    public function updateUserInfo()
    {

        $admin_id = $this->input->post('admin_id');

        $model_data = array(
            'admin_id' => $admin_id,
            'e_mail' => $this->input->post('e_mail'),
            'f_name' => $this->input->post('f_name'),
            'l_name' => $this->input->post('l_name'),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
            'city' => $this->input->post('city'),
            'postal_code' => $this->input->post('postal_code'),
            'state' => $this->input->post('state'),
            'name_of_organisation' => $this->input->post('name_of_organisation'),
            'establish_year' => $this->input->post('establish_year'),
            'intial_investment' => $this->input->post('intial_investment'),
            'registration_no' => $this->input->post('registration_no'),
            'aadhar_no' => $this->input->post('aadhar_no')
        );

        if($this->adminmodel->updateUserInfo($model_data)){
            echo json_encode(true);
        }


        // $this->session->set_flashdata('success_msg', 'Basic info updated');
        // redirect('admin/account/basic_info');
    }

  
    public function updatePassword()
    {
        $admin_id = $this->input->post('admin_id');
        $password = $this->input->post('password');
      
        $model_data = array(
            'admin_id' => $admin_id,
        );
        $model_data['password'] = password_hash($password, PASSWORD_BCRYPT);
        if($this->adminmodel->updatePassword($model_data)){
              echo json_encode(true);
         }
     }
   
    
     public function updateDonarInfo()
    {
        $donor_id = $this->input->post('id');
        
        $model_data = array(
            'id' => $donor_id,
            'pageName' => 'Donar',
            'phone_no' => $this->input->post('phone_no'), 
            'email' => $this->input->post('email'), 
            'address' => $this->input->post('address'),
            'country' => $this->input->post('country'), 
            'state' => $this->input->post('state'),  
            'city' => $this->input->post('city'), 
            'zipcode' => $this->input->post('zipcode'), 
            'amount' => $this->input->post('amount'),
            'ip_address' => $this->input->post('ip_address'),
            'status'=>$this->input->post('active')  
        );
print_r($model_data);
exit();
        if($this->adminmodel->updateDonarInfo($model_data)){
            echo json_encode(true);
        }
        // $this->session->set_flashdata('success_msg', 'Basic info updated');
        // redirect('admin/account/basic_info');
    }


     public function updateBankInfo()
    {
        $admin_id = $this->input->post('admin_id');
        
        $model_data = array(
            'admin_id' => $admin_id,

            'account_no' => $this->input->post('account_no'),
            'acount_holder_name' => $this->input->post('acount_holder_name'),
            'bank_name' => $this->input->post('bank_name'),
            'ifsc_code' => $this->input->post('ifsc_code'),
            'type' => $this->input->post('type')
        );
        $this->adminmodel->updateBankDetails($model_data);
        $this->session->set_flashdata('success_msg', 'Bank info updated');
        redirect('admin/account/bank_details');
    }


    public function updateUserPasswordInfo()
    {
        print_r('expression');
        exit();
        $party_id = $this->input->post('party_id');
        $model_data = array(
            'party_id' => $party_id,
            'new_password' => $this->input->post('new_password'),
            'con_new_password' => $this->input->post('con_new_password'),
            );
        if ($model_data['new_password'] == $model_data['con_new_password']) {
            $this->adminmodel->updateUserPasswordInfo($model_data);
            $this->session->set_flashdata('success_msg', 'Password updated');
        } else {
            $this->session->set_flashdata('error_msg', 'Pasword not match');
        }
        redirect('admin/account/password');
    }

    public function updateRateTable()
    {  
        $model_data = array(
            'gold' => $this->input->post('gold'),
            'silver' => $this->input->post('silver'),
        );
        if($this->adminmodel->updateRate($model_data))
        {
             echo json_encode(true);
        }
    }

    public function getBankDetails()
    {  
        $model_data = array(
            'gold' => $this->input->post('gold'),
            'silver' => $this->input->post('silver')
        );
        $this->adminmodel->updateRate($model_data);
        $this->session->set_flashdata('success_msg', 'Rate Table Updated');
        redirect('admin/account/rate_table');
    }


    // Update Admin Profile Picture
    public function updateAdminProfilePicture()
    { 
        $party_id = $this->input->post('party_id');

        $config['upload_path'] = './images/profile/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('image')) {
            $data1 = array('upload_data' => $this->upload->data());
            $model_data = array(
                'PROFILE_IMG' => base_url()."images/profile/".$data1['upload_data']['file_name'], 
                );
            $this->adminmodel->editPartyAdminProfilePicture($party_id, $model_data['PROFILE_IMG']);
            $this->session->set_flashdata('success_msg', 'Profile updated');
            redirect('admin/account/profilepicture');
        } else {
            $this->session->set_flashdata('error_msg', 'Profile not updated');
            redirect('admin/account/profilepicture');
        }
    }




    public function userProfilePicture()
    {
        $admin_id = $this->input->post('admin_id');

        $fileName = $this->image_resize_model->resizeCustomImage('images/profile/', 600);

        $data = array(
            'admin_id' => $admin_id,
            'admin_img' => $fileName,
        );
        $this->adminmodel->updateUserInfo($data);

        $this->session->set_flashdata('success_msg', 'Profile updated');
        redirect('admin/account/profile_image');
    }

    

}