<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('bankmodel');
    }


    function fetch_details()
    {   
        $fetch_data = $this->bankmodel->make_datatables();
        $data = array();
        $no=0;
        foreach($fetch_data as $bank)  
        { 
            $sub_array = array();
            $no+=1;
            $bank_id=$bank->bank_id;
            $account_no=$bank->account_no;
            $account_holder_name=$bank->account_holder_name;
            $bank_name=$bank->bank_name;
            $ifsc_code=$bank->ifsc_code;
            $type=$bank->type;
            $status=$bank->status;

            // if(!empty($user->merchant_id))
            //     $user_merchant_id=$user->merchant_id;
            // else
            //     $user_merchant_id="N/A";
            
            // $user_status = '<small class="label label-success bg-none">'.$user->status.'</small>';
            // $user_payment_method=$user->payment_method;
            // $user_update_time=$user->update_time;

            $sub_array = array(
                $no,
                $account_no."<br>".$account_holder_name."<br>".$bank_name." (".$ifsc_code.")",
                $type,
                $status,
                $user_action='<a href="javascript:void(0);" onclick="openBankModal('.$bank_id.');" class="btn btn-primary"><i class="fa fa-edit"></i></a>',
            );

            $data[] = $sub_array; 
        }  
        $output = array(  
            "draw"            =>   intval($_POST["draw"]),  
            "recordsTotal"    =>   $this->bankmodel->get_all_data(),  
            "recordsFiltered" =>   $this->bankmodel->get_filtered_data(),  
            "data"            =>   $data  
        );  
        echo json_encode($output);  
    }

    public function getbBankDetail()
    {
        $bank_id = $this->input->post('bank_id');
        $bank_info = $this->bankmodel->getbBankDetail($bank_id);
        echo json_encode($bank_info);  
    }

    public function updateBankDetails()
    {
        $data = array(
            'bank_id' => $this->input->post('bank_id'),
            'account_no' => $this->input->post('bank_id'),
            'account_holder_name' => $this->input->post('bank_id'),
            'bank_name' => $this->input->post('bank_id'),
            'ifsc_code' => $this->input->post('bank_id'),
            'type' => $this->input->post('bank_id'),
        );
        
        $bank_info = $this->bankmodel->updateBankDetails($data);
        $this->session->set_flashdata('success_msg', 'Bank detail updated');
        redirect('admin/account/bank_details');

    }








}