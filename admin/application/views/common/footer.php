</div>
<!-- getting this scripts from karyon_config.php file which is under application > config folder -->
<?php
foreach ($scripts['foot'] as $file) {
    echo "<script src='$file'></script>";
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sweetalert/sweetalert2.css">
<script src="<?php echo base_url();?>assets/sweetalert/sweetalert2.all.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/sweetalert/sweetalert2.js" type="text/javascript"></script>

<script type="text/javascript">
    var site_url = "<?php echo site_url(); ?>";
    var base_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url();?>assets/account/bank.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/account/basic_info.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/account/rate.js" type="text/javascript"></script>



<script type="text/javascript">
   function submitTagForm()
    {

      var subcat = $('.my_select_box');
        $.ajax({
            url: '<?php echo site_url(); ?>/admin/submitTagForm',
            type: 'POST',
            data: $('#add_tag_form').serialize(), 
            success: function (response) {
                toastr.success('Tag Added');

                $("#addTag").modal('hide');
                   subcat.html(response);
                   subcat.trigger('chosen:updated');

                // $("#tag_section").load(location.href + " #tag_section"); 
            },
            error: function () {
                alert('An error has occurred');
            }
        });

        // location.href = '<?php echo site_url(); ?>/admin/addBlogPage';
    }
</script>
<script>
    $(function () {
        $("#WYSIHTML").wysihtml5();
        $("#WYSIHTML1").wysihtml5();
        $("#WYSIHTML2").wysihtml5();
        $("#WYSIHTML3").wysihtml5();
        $("#WYSIHTML5").wysihtml5();
        $("#WYSIHTML6").wysihtml5();
        $("#WYSIHTML7").wysihtml5();
        $("#WYSIHTML8").wysihtml5();
        $("#WYSIHTML9").wysihtml5();
    });

    //Date range picker
    $('.datepicker').daterangepicker();

    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function(){
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
 
</script>

    <script>
         $(function () {
            $(".select2").select2();
         });
     </script>
<script>
    $(function () {
        $('#blogListTable').DataTable({
            order: [[4, 'desc']],
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
        $("#example3").DataTable();
    });
</script>



  <!-- Start notification toster -->
<?php if ($this->session->flashdata('message') != ""){ ?>
<script>
    toastr.info('<?php echo $this->session->flashdata('message');?>');
</script>
<?php } ?> 
<!-- End Notification toster -->

<!-- Start notification toster -->
<?php if ($this->session->flashdata('success_msg') != ""){ ?>
<script>
    toastr.success('<?php echo $this->session->flashdata('success_msg');?>');
</script>
<?php } ?> 
<!-- End Notification toster -->

<!-- Start notification toster -->
<?php if ($this->session->flashdata('error_msg') != ""){ ?>
<script>
    toastr.error('<?php echo $this->session->flashdata('error_msg');?>');
</script>
<?php } ?> 
<!-- End Notification toster -->

</body>
</html>
