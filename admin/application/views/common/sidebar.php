<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?php echo site_url('admin'); ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo"><b>Sadhana</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"> 
                  Sadhana
                    <!-- <img class="img-responsive" src="<?php echo base_url(); ?>images/logo.png" style="height: 60px;width: 230px;">  -->
                </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="<?php echo site_url('login/logout'); ?>"> 
                                <i class="fa fa-sign-out" aria-hidden="true"></i> Sign out
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <?php
        $this->userInfo = $this->session->admin_login_data;
        $user_type = $this->userInfo['admin_type']; 
        ?>
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar" style="height: auto;">
                <div class="user-panel" style="padding-bottom: 30px;">
                    <div class="pull-left image">
                        <?php if (empty($data['adminBasicInfo']->admin_img)) { ?>
                            <img class="profile-user-img img-responsive" src="<?php echo base_url(); ?>assets/images/profile_img.jpg" alt="User profile picture" height="45px;">
                        <?php } else { ?>
                            <img class="profile-user-img img-responsive" src="<?php echo base_url()."images/profile/".$data['adminBasicInfo']->admin_img; ?>" alt="User profile picture" height="45px;">
                        <?php } ?>
                    </div>
                    <div class="pull-left info">
                        <p>Admin</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- Sidebar user panel -->
                <ul class="sidebar-menu">

                    <li class="header">MAIN NAVIGATION</li>

                    <?php echo($data['pageName'] == "DASHBOARD" ? "<li class='active'>" : "<li>"); ?>
                        <a href="<?php echo base_url(); ?>index.php/admin" onfocus="top">
                            <i class="fa fa-user"></i> <span>Dashboard</span>
                        </a>
                    <?php echo($data['pageName'] == "ACCOUNT" ? "<li class='active'>" : "<li>"); ?>
                        <a href="<?php echo base_url(); ?>index.php/admin/account">
                            <i class="fa fa-user"></i> <span>Account</span>
                        </a>

                    <?php  echo($data['pageName']=="DONAR" ? "<li class='active'>" : "<li>"); ?>  
                        <a href="<?php echo base_url();?>index.php/admin/donor">
                            <i class="fa fa-user"></i><span>Donar</span>
                        </a>
                    </li>
                </ul>
            </section>
                <!-- /.sidebar -->
        </aside>