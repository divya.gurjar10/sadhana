<style type="text/css">
.easy-autocomplete-container {
    left: 0;
    position: absolute;
    width: 100%;
    z-index: 999;
}
</style>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.css"> -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>Blog</h1>
        <a href="<?php echo base_url(); ?>index.php/admin/blog" class="btn btn-default float__right" style="margin-top: -30px; font-weight: bold;">
            <i class="fa fa-reply"></i>
        </a>
   </section>
    <section class="content ">
        <form name="createBlog" id="createBlog" class="form-horizontal" action="<?php echo base_url(); ?>index.php/admin/addBlog" method="POST"  enctype="multipart/form-data">
            
            <div class="box box-primary">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-12">
                                    <label class="control-label asterisk">Title</label>
                                            <input type="text" class="form-control" name="title" value="" required="required">
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-12">
                                    <label class="control-label asterisk">Description</label>
                                        <textarea class="form-control" id="WYSIHTML" name="description" required="required"></textarea>
                                        <label id="description-error" class="error" for="description" style="display: none;">Please enter description</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-10">
                                        <label class="control-label asterisk">Tags</label>
                                        <div id="tag_section1">

                                            <select class="select2 my_select_box" id="tag_section" multiple="multiple" name="tags[]" style="width: 100%;">

                                                <?php 
                                                    if(isset($data['tags']) && !empty($data['tags'])) { 
                                                        foreach($data['tags'] as $tag) {
                                                ?>
                                                    <option value="<?php echo $tag->id?>"><?php echo $tag->name?></option>
                                                <?php } } ?>
                                            </select>
                                        <label id="tags-error" class="error" for="tags" style="display: none;">Please select a tag</label>
                                        </div>
                                        <!-- <input type="text" class="form-control" id="tag_name" name="tags" value="" style="border-radius: 0;"> -->
                                    </div>

                                    <div class="col-md-2" style="padding: 24px 0 0 18px;">
                                        <button data-toggle="modal" data-target="#addTag" type="button" class="btn btn-prmary-background font_bold" style="color: #000">Add tag</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-12">
                                        <label class="control-label asterisk">Banner Image</label>


                                        <input type="file" name="image" class="file">
                                        <div class="input-group col-xs-12">
                                          <span class="input-group-addon"><i class="fa fa-file-image-o"></i></span>
                                          <input type="text" class="form-control input-lg bg_white" disabled="disabled" placeholder="Upload Image" value="">
                                          <span class="input-group-btn">
                                            <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <br>

                            <div class="walk_request_button">
                                <button type="submit" class="btn btn-prmary-background font_bold">Add</button>

                            </div>
                        </div>
                        <div class="col-md-2">

                        </div>
                    </div>
                </div>
            </div>
        </form> 


    </section>
</div>
<div id="addTag" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Tag</h4>
            </div>
            <div class="modal-body">
                <form name="createTag" method="post" id="add_tag_form">
                    <input type="hidden" name="form_type" value="ForAdd">
                    <div class="row">
                        <div class="form-group col-sm-12 col-md-12">
                            <div class="col-md-12">
                                <label class="control-label asterisk">Tag</label>
                                <input type="text" class="form-control" name="name" required="required">
                            </div>
                        </div>                    
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="$('#add_tag_form').submit()">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



<!-- <script type="text/javascript">
    var options = {

      url: "<?php echo site_url(); ?>/admin/ajaxAllTags",

      getValue: "tag_name",

      list: {   
        match: {
          enabled: true
        }
      },

      theme: "square"
    };

$("#tag_name").easyAutocomplete(options);
</script> -->