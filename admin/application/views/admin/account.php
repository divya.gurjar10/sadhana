

<!--
    KARYON SOLUTIONS CONFidENTIAL
    __________________
    
      Author : Ashwin Choudhary
      Url - http://www.karyonsolutions.com  
      [2016] - [2017] Karyon Solutions 
      All Rights Reserved.
    
    NOTICE:  All information contained herein is, and remains
    the property of Karyon Solutions and its suppliers,
    if any.  The intellectual and technical concepts contained
    herein are proprietary to Karyon Solutions
    and its suppliers and may be covered by Indian and Foreign Patents,
    patents in process, and are protected by trade secret or copyright law.
    Dissemination of this information or reproduction of this material
    is strictly forbidden unless prior written permission is obtained
    from Karyon Solutions.
    -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>User Profile</h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i>admin</a></li>
            <li class="active">User profile</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <?php if (!empty($data['adminBasicInfo']->admin_img)) { ?>
                        <img class="profile-user-img img-responsive img-circle"
                            src="<?php echo base_url()."images/profile/".$data['adminBasicInfo']->admin_img; ?>"
                            alt="User profile picture" width="300px" height="300px" style="height: 92px;">
                        <?php } else { ?>
                        <img class="profile-user-img img-responsive img-circle"
                            src="<?php echo base_url();?>assets/images/profile.jpg"
                            alt="User profile picture" width="300px" height="300px" style="height: 92px;">
                        <?php } ?>
                        <h3 class="profile-username text-center">
                            <?php echo($data['adminBasicInfo']->f_name); ?>
                        </h3>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Name : </b> <a class="pull-right">
                                <?php echo $data['adminBasicInfo']->f_name." ".$data['adminBasicInfo']->l_name;?> 
                                </a>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="<?php if($data['active'] == "basic_info") echo "active"; ?>"><a href="#editBasicInfo" data-toggle="tab">Basic Info</a></li>

                        <li class="<?php if($data['active'] == "profile_image") echo "active"; ?>"><a href="#editprofile" data-toggle="tab">Profile Image</a></li>
                        <li class="<?php if($data['active'] == "password_info") echo "active"; ?>"><a href="#editPassword" data-toggle="tab">Change Password</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <?php if ($data['active'] == "profile_image") echo "active"; ?>" id="editprofile" enctype="multipart/form-data">
                            <form name="updateprofileimage" class="form-horizontal" action="<?php echo site_url('admin/userProfilePicture');?>"
                                method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="admin_id" value="<?php echo $data['admin_id'];?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="file" name="image">
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-prmary-background font_bold" style="margin-top: 1%;">Update</button>
                            </form>
                        </div>
                        <div class="tab-pane <?php if ($data['active'] == "bank_details") echo "active"; ?>" id="password">
                            <!-- <form name="" class="form-horizontal" action="<?php echo base_url(); ?>index.php/admin/updateBankInfo"
                                method="POST">
                                <input type="hidden" name="party_id"
                                    value="<?php if (!empty($data['admin_id'])) echo $data['admin_id']; ?>"> -->
                                <section class="content">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box box-primary">
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="user_list table-responsive">
                                                              
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- <div class="modal-footer">
                                    <button type="submit" onclick='' class="btn btn-prmary-background font_bold">Update</button>
                                </div> -->
                            <!-- </form> -->
                        </div>
                        <div class="tab-pane <?php if ($data['active'] == "basic_info") echo "active"; ?>" id="editBasicInfo">
                            <form name="updatebasicinfo" class="form-horizontal" action="javascript:void(0);"
                                method="POST" id="updatebasicinfo">
                                <input type="hidden" name="admin_id" value="<?php echo $data['admin_id'];?>">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Email (username)</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo'])) { ?>
                                        <input type="text" class="form-control" name="e_mail"
                                            value="<?php if (!empty($data['adminBasicInfo']->e_mail)) echo $data['adminBasicInfo']->e_mail; ?>" required readonly/>
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="user_login_id" required="required"/>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">First Name</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->f_name)) { ?>
                                        <input type="text" class="form-control" name="f_name"
                                            value="<?php if (!empty($data['adminBasicInfo']->f_name)) echo $data['adminBasicInfo']->f_name; ?>"
                                            required="required" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="f_name" required="required" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Last Name</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->l_name)) { ?>
                                        <input type="text" class="form-control" name="l_name"
                                            value="<?php if (!empty($data['adminBasicInfo']->l_name)) echo $data['adminBasicInfo']->l_name; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="l_name" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Phone</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->phone)) { ?>
                                        <input type="text" class="form-control" name="phone"
                                            value="<?php if (!empty($data['adminBasicInfo']->phone)) echo $data['adminBasicInfo']->phone; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="phone" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Name Of Organisation</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->name_of_organisation)) { ?>
                                        <input type="text" class="form-control" name="name_of_organisation"
                                            value="<?php if (!empty($data['adminBasicInfo']->name_of_organisation)) echo $data['adminBasicInfo']->name_of_organisation; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="name_of_organisation" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Establish Year</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->establish_year)) { ?>
                                        <input type="date" class="form-control" name="establish_year"
                                            value="<?php if (!empty($data['adminBasicInfo']->establish_year)) echo $data['adminBasicInfo']->establish_year; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="establish_year" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Intial Investment</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->intial_investment)) { ?>
                                        <input type="text" class="form-control" name="intial_investment"
                                            value="<?php if (!empty($data['adminBasicInfo']->intial_investment)) echo $data['adminBasicInfo']->intial_investment; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="intial_investment" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Registration No.</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->registration_no)) { ?>
                                        <input type="text" class="form-control" name="registration_no"
                                            value="<?php if (!empty($data['adminBasicInfo']->registration_no)) echo $data['adminBasicInfo']->registration_no; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="registration_no" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">City</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->city)) { ?>
                                        <input type="text" class="form-control" name="city"
                                            value="<?php if (!empty($data['adminBasicInfo']->city)) echo $data['adminBasicInfo']->city; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="city" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">State</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->state)) { ?>
                                        <input type="text" class="form-control" name="state"
                                            value="<?php if (!empty($data['adminBasicInfo']->state)) echo $data['adminBasicInfo']->state; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="state" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Address</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->address)) { ?>
                                        <input type="text" class="form-control" name="address"
                                            value="<?php if (!empty($data['adminBasicInfo']->address)) echo $data['adminBasicInfo']->address; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="address" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Postal Code</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->postal_code)) { ?>
                                        <input type="text" class="form-control" name="postal_code"
                                            value="<?php if (!empty($data['adminBasicInfo']->postal_code)) echo $data['adminBasicInfo']->postal_code; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="postal_code" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Aadhar No.</label>
                                    <div class="col-md-10">
                                        <?php if (isset($data['adminBasicInfo']->aadhar_no)) { ?>
                                        <input type="text" class="form-control" name="aadhar_no"
                                            value="<?php if (!empty($data['adminBasicInfo']->aadhar_no)) echo $data['adminBasicInfo']->aadhar_no; ?>" required="required">
                                        <?php } else { ?>
                                        <input type="text" class="form-control" name="aadhar_no" required="required">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-prmary-background font_bold">Update</button>
                                </div>
                            </form>
                        </div>


                         <div class="tab-pane <?php if ($data['active'] == "password_info") echo "active"; ?>" id="editPassword">
                            <form name="changePassword" class="form-horizontal" action="javascript:void(0);"
                                method="POST" id="changePassword" >
                                <input type="hidden" name="admin_id" value="<?php echo $data['admin_id'];?>">
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Password</label>
                                    <div class="col-md-10">
                                     <input type="password" class="form-control" name="password" required="required" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label asterisk">Re-Password</label>
                                    <div class="col-md-10">
                                        <input type="password" class="form-control" name="re-password" required="required" >
                                    </div>
                                </div>
                                 <div class="modal-footer">
                                    <button class="btn btn-prmary-background font_bold">Update</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
