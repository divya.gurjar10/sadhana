<div class="content-wrapper">
  <section class="content-header">
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> admin</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-4  col-xs-6 ">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
           
   <h3> <?php  $TotalDonation = $this->adminmodel->countTotalDonation();
    print_r($TotalDonation->sum);?>
   </h3>
          <p> <b>Total donations received </b> </p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo $this->db->count_all('donors');?><sup style="font-size: 20px"></sup></h3>
          <p>Total number of donations</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
        
            <h3><?php  $CountVcantCards = $this->adminmodel->CountVcantCards();
    print_r($CountVcantCards->count);?></h3>

          <p>No of Vacant Cards</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->

    <!-- ./col -->
  </div>
</div>
</div>