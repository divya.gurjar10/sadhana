<style type="text/css">
    .tag-lists a {
        display: inline-block;
        padding: 8px 15px;
        border: 1px solid #eaeaea;
        margin-right: 15px;
        margin-bottom: 15px;
        transition: all 500ms ease;
        -moz-transition: all 500ms ease;
        -webkit-transition: all 500ms ease;
        -ms-transition: all 500ms ease;
        -o-transition: all 500ms ease;
        border-radius: 3px;
        font-family: "Open Sans", sans-serif;
        color: #6d6d6d;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Blog</h1>
                <a href="<?php echo base_url(); ?>index.php/admin/blog" class="btn btn-default float__right" style="margin-top: -30px; font-weight: bold;">
                    <i class="fa fa-reply"></i>
                </a>
        <!-- <ol class="breadcrumb">
            <li>
      </li>
      </ol> -->
   </section>
    <section class="content ">
        <form name="updateBlog" id="updateBlog" class="form-horizontal" action="<?php echo base_url(); ?>index.php/admin/updateBlog" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="blog_id" value="<?php echo $data['blog_id']; ?>">
            <input type="hidden" name="img_name" value="<?php echo $data['blogInfo']->img_name; ?>">
            <div class="box box-primary">

                <div class="box-body">

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-12">
                                    <label class="control-label asterisk">Title</label>
                                            <input type="text" class="form-control" name="title" value="<?php echo($data['blogInfo']->title);?>" required="required">
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-12">
                                    <label class="control-label asterisk">Description</label>
                                            <textarea id="WYSIHTML" class="form-control" name="description" required="required"><?php echo($data['blogInfo']->description);?></textarea>
                                        <label id="description-error" class="error" for="description" style="display: none;">Please enter description</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-10">
                                    <label class="control-label ">Tags</label>
                                        <?php 
                                            $blog_tag_id = array();
                                            foreach($data['blogTags'] as $blogTag) 
                                            { 
                                                $blog_tag_id[] = $blogTag->tag_id;
                                            } 
                                        ?>


                                        <select class="select2 my_select_box" id="tag_section" multiple="multiple" name="tags[]" style="width: 100%;">
                                            <?php 
                                                if(isset($data['tags']) && !empty($data['tags'])) { 
                                                    foreach($data['tags'] as $tag) {
                                            ?>

                                            <option value="<?php echo $tag->id;?>" 
                                                <?php 
                                                    if(in_array($tag->id, $blog_tag_id)) 
                                                        { 
                                                            echo "selected"; 
                                                        } 
                                                    ?> 
                                                >
                                                <?php echo $tag->name; ?>
                                            </option>
                                            <?php } } ?>
                                        </select>
                                        <label id="tags-error" class="error" for="tags" style="display: none;">Please select a tag</label>
                                    </div>

                                    <div class="col-md-2" style="padding: 24px 0 0 18px;">
                                        <button data-toggle="modal" data-target="#addTag" type="button" class="btn btn-prmary-background font_bold" style="color: #000">Add tag</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-md-12">
                                    <div class="col-md-12">
                                        <label class="control-label">Banner Image</label>


                                        <input type="file" name="blog_image" class="file">
                                        <div class="input-group col-xs-12">
                                          <span class="input-group-addon"><i class="fa fa-file-image-o"></i></span>
                                          <input type="text" class="form-control input-lg bg_white" disabled="disabled" placeholder="Upload Image" value="<?php if(isset($data['blogInfo']->img_name) && !empty($data['blogInfo']->img_name)) { echo($data['blogInfo']->img_name); }?>">
                                          <span class="input-group-btn">
                                            <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                                          </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <br>

                            <div class="walk_request_button">
                                <button type="submit" class="btn btn-prmary-background font_bold">Update</button>

                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </form> 


    </section>
</div>

<div id="addTag" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Tag</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="add_tag_form">
                    <input type="hidden" name="form_type" value="ForUpdate">
                    <input type="hidden" name="blog_id" value="<?php echo($data['blog_id']); ?>">
                    <div class="row">
                        <div class="form-group col-sm-12 col-md-12">
                            <div class="col-md-12">
                            <label class="control-label asterisk">Tag</label>
                                <input type="text" class="form-control" name="name" value="" required="required">
                            </div>
                        </div>                    
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="submitTagForm()">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
