<div class="content-wrapper">
    <section class="content-header">
    <h1>Blogs</h1>

        <a href="<?php echo base_url(); ?>index.php/admin/blog" class="btn btn-default float__right" style="margin-top: -30px; font-weight: bold;">
            <i class="fa fa-reply"></i>
        </a>
        <!-- <ol class="breadcrumb">
            <li>
            </li>
        </ol> -->
    </section>
  <!-- <?php if ($data['active'] == "indivisual") echo "active"; ?> -->
    <section class="content ">
    
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">

                        <?php if(!empty($data['blogData']->img_name)) { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <img src="<?php echo base_url();?>/images/blogs/<?php echo($data['blogData']->img_name);?>" style="width: 300px;">
                                </div>
                            </div>
                        </div>
                        <br>
                        <?php }?>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="col-md-2">
                                    <label>Title : </label>
                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <?php if(!empty($data['blogData']->title)) echo $data['blogData']->title; ?>
                                    </p>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="col-md-2">
                                    <label>Tags : </label>
                                </div>
                                <div class="col-md-10">


                                    <p>
                                        <?php
                                        if(!empty($data['blogTags'])) 
                                        {
                                            $numItems = count($data['blogTags']);
                                            $i = 0;
                                           foreach ($data['blogTags'] as $tag) 
                                           {
                                                if($tag->name != "") {
                                        ?>

                                        <?php
                                            echo("#".$tag->name); 
                                                if(++$i !== $numItems){ echo " ";}
                                        ?> 
                                    <?php } } } ?> 
                                    </p>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-2">
                                    <label>Date : </label>
                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <?php echo date('d M Y', strtotime($data['blogData']->created_date)); ?>
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <label>Description : </label>
                                    <p>
                                        <?php if(!empty($data['blogData']->description)) echo $data['blogData']->description; ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </section>
</div>
