<div class="content-wrapper">
    <section class="content-header">
    <h1>Blogs</h1>
        <!-- <ol class="breadcrumb">
            <li> -->
                <a href="<?php echo base_url(); ?>index.php/admin/addBlogPage" class="btn btn-prmary-background float__right font_bold" style="margin-top: -30px;color: #000">
                    Add Blog
                </a>
            <!-- </li>
        </ol> -->
    </section>
  <!-- <?php if ($data['active'] == "indivisual") echo "active"; ?> -->
    <section class="content ">
        <div id="blog_section">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive">
                            <table id="blogListTable" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width: 15%;">Image</th>
                                        <th style="width: 15%;">Tag</th>
                                        <th style="width: 15%;">Title</th>
                                        <th style="width: 30%;">Description</th>
                                        <th style="width: 10%;">Date</th>
                                        <th style="width: 10%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(isset($data['blogList']) && !empty($data['blogList'])) 
                                        { 
                                            foreach($data['blogList'] as $blog)
                                            {
                                                $blog_tags = $this->adminmodel->getBlogTags($blog->blog_id);
                                    ?>
                                        <tr>
                                            <td>
                                                <?php if(isset($blog->img_name) && !empty($blog->img_name)) { ?>
                                                    <img src="<?php echo base_url();?>images/blogs/<?php echo $blog->img_name; ?>" style="width: 50%;">
                                                <?php } else { echo("NA"); }?>
                                            </td>
                                            <td>       
                                                <?php
                                                    if(isset($blog_tags) && !empty($blog_tags))
                                                    {                                                      
                                                        $numItems = count($blog_tags);
                                                        $i = 0;
                                                        foreach($blog_tags as $tag)  {
                                                            if($tag->name != "") {
                                                            echo("#".$tag->name); 
                                                                if(++$i !== $numItems){ echo " ";}
                                                ?>
                                                <?php } } } ?>
                                                <!-- <?php
                                                    $str="foo,bar,baz,bat";
                                                    $arr=explode(",",$str);
                                                    print_r($arr);
                                                ?> -->
                                                <!-- <?php if(isset($blog->tags) && !empty($blog->tags)) { echo $blog->tags; } else { echo("-"); } ?> -->
                                            </td>
                                            <td>
                                                <?php echo $blog->title; ?>                                            
                                            </td>
                                            <td>
                                                <?php 
                                                    if (strlen($blog->description) < 60)
                                                        echo $blog->description;
                                                    else
                                                        echo substr($blog->description, 0, 150) . " . . . ";
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo date('d M Y', strtotime($blog->created_date)); ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-info" href="<?php echo site_url();?>/admin/blogOverview?blog_id=<?php echo($blog->blog_id);?>">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-danger" href="javascript:void(0)" onclick="deleteBlog(<?php echo($blog->blog_id);?>)">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <a class="btn btn-success" href="<?php echo site_url();?>/admin/updateBlogPage?blog_id=<?php echo($blog->blog_id);?>">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
</div>

<script type="text/javascript">
        
    function deleteBlog(blog_id)
    {


        swal({
            title: 'Are you sure?',
            text: "You want to delete this blog",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete it!'
        }).then((result) =>
        {
            if (result.value)
            {
                $.ajax({
                    url: '<?php echo site_url(); ?>/admin/deleteBlog',
                    type: 'GET',
                    data: {
                        blog_id: blog_id
                    }, 
                    success: function (response) {
                        // $("#blog_section").load(location.href + " #blog_section"); 
                        toastr.success('Blog deleted');


                        var delayInMilliseconds = 1000; //1 second

                        setTimeout(function() {
                            location.href = '<?php echo site_url(); ?>/admin/blog'; 
                        }, delayInMilliseconds);
                    },
                    error: function () {
                        alert('An error has occurred');
                    }
                });
            }
        })

    }
</script>