<div class="content-wrapper">
    <section class="content-header">
    <h1>Donor Detail</h1>
    </section>
        <section class="content ">
        <div id="blog_section">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body table-responsive">
                            
                                  
                               
                            <table id="blogListTable" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">Name</th>
                                        <th style="width: 10%;">Phone No</th>
                                        <th style="width: 10%;">Email</th>
                                        <th style="width: 15%;">Address</th>
                                        <th style="width: 15%;">City</th>
                                        <th style="width: 15%;">State</th>
                                        <th style="width: 15%;">Country</th>
                                        <th style="width: 10%;">ZipCode</th>
                                        <th style="width: 10%;">Amount</th>
                                        <th style="width:10%;">IP Address</th>
                                        <!-- <th style="width: 10%;">Action</th> -->

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(isset($data['donorList']) && !empty($data['donorList'])) 
                                         { 
                                            foreach($data['donorList'] as $rows)
                                             {
                                                $blog_tags = $this->adminmodel->getDonorInfo($rows->id);
                                    ?>
                                     <tr>
                                        <td>
                                            <?php echo $rows->name; ?>  
                                        </td>
                                        <td>
                                            <?php echo $rows->phone_no; ?>                                            
                                        </td>
                                        <td>
                                            <?php echo $rows->email; ?>                                            
                                        </td>
                                        <td>
                                            <?php 
                                                if (strlen($rows->address) < 60)
                                                    echo $rows->address;
                                                else
                                                    echo substr($rows->address, 0, 150) . " . . . ";
                                            ?>
                                        </td>
                                        <td>
                                             <?php echo $rows->city; ?>            
                                        </td>
                                        <td>
                                            <?php echo $rows->state; ?>            
                                        </td>
                                         <td>
                                            <?php echo $rows->country; ?>                                            
                                        </td>
                                        <td>
                                            <?php echo $rows->zipcode; ?>                                            
                                        </td>
                                        <td>
                                            <?php echo $rows->amount; ?>                                            
                                        </td>
                                        <td>
                                            <?php 
                                                echo $rows->ip_address;
                                            
                                            ?> 

                                        </td>
                                            <!-- <td>
                                                <a class="btn btn-info" href="<?php echo site_url();?>/admin/donorOverview?donor_id=<?php echo($rows->donor_id);?>">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-danger" href="javascript:void(0)" onclick="deleteRows(<?php echo($rows->donor_id);?>)">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <a class="btn btn-success" href="<?php echo site_url();?>/admin/updatedonorPage?donor_id=<?php echo($rows->donor_id);?>">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td> -->
                                        </tr>
                                    <?php } } ?>

                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
</div>
                       
<script type="text/javascript">
        
    function deleteDonor(donor_id)
    {


        swal({
            title: 'Are you sure?',
            text: "You want to delete this donor",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete it!'
        }).then((result) =>
        {
            if (result.value)
            {
                $.ajax({
                    url: '<?php echo site_url(); ?>/admin/deleteDonor',
                    type: 'GET',
                    data: {
                        donor_id: donor_id
                    }, 
                    success: function (response) {
                        // $("#blog_section").load(location.href + " #blog_section"); 
                        toastr.success('Donor deleted');


                        var delayInMilliseconds = 1000; //1 second

                        setTimeout(function() {
                            location.href = '<?php echo site_url(); ?>/admin/donor'; 
                        }, delayInMilliseconds);
                    },
                    error: function () {
                        alert('An error has occurred');
                    }
                });
            }
        })

    }
</script>