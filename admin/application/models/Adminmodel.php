<?php

Class Adminmodel extends CI_Model
{   
    public $userInfo = null;
    public $login_party_id = null;

    function __construct()
    {
        parent::__construct();
        $this->userInfo = $this->session->admin_login_data;
    }


    /**
     * Function for getting users info
     * @param $party_id
     * @return mixed
     */
    public function getUserBasicInfo($party_id) 
    {
        $this->db->select('*');
        $this->db->from('user_login');

        if(!$this->db->get()) {
            show_error('Error in getting basic info');
            die();
        }
        else {
            $this->db->select('*');
            $this->db->from('admin');
            $row = $this->db->get()->row();
            return $row;
        }

    }

   public function getPasswordInfo() 
    {
        $this->db->select('email,password');
        $this->db->from('user_login');

        if(!$this->db->get()) {
            show_error('Error in getting password ');
            die();
        }
        else {
            $this->db->select('e_mail,password');
            $this->db->from('admin');
            $row = $this->db->get()->row();
            return $row;

        }
        print_r('expression');
        exit();
    }
    

    public function getDonorInfo($donar_id) 
    {
        
        $this->db->select('*');
        $this->db->from('donors');

        if(!$this->db->get()) {
            show_error('Error in getting donar info');
            die();
        }
         else {
            $this->db->select('*');
            $this->db->from('admin');
            $row = $this->db->get()->row();
            return $row;
        }
 //   return array("fetch"=>$result,"num_rows"=>$num_rows); 
           
    }

    public function getRate() 
    {
        $this->db->select('*');
        $this->db->from('rate_table');
        return $this->db->get()->row();
    }

    //Donor 
    public function getDonorList() 
    {
       $this->db->select('*');
       $this->db->from('donors');
       return $this->db->get()->result();
      
    }

    public function CountVcantCards()
    {
        $this->db->select('COUNT(id) as count');
        $this->db->from('cards');
        $this->db->where('active', 1);
       return $this->db->get()->row(); 

    }

    public function countTotalDonation()
    {
        $this->db->select('SUM(card_value) as sum');
        $this->db->from('cards');
        $this->db->where('active', 0);
       return $this->db->get()->row(); 
  
    }
    
    public function getBankDetails() 
    {
        $this->db->select('*');
        $this->db->from('bank_details');
        return $this->db->get()->result();
    }

    public function getTags()
    {
        $this->db->select('*');
        $this->db->from('tags');
        $this->db->where('deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog tag list');
            die();
        }
        else {
            $this->db->select('*');
            $this->db->from('tags');
            $this->db->where('deleted_date', '0000-00-00');
            $result = $this->db->get()->result();
            return $result;
        }
    }



    /**
     * Function for getting Bolg list
     * @return mixed
     */
    public function countBlogList()
    {
        $this->db->select('blogs.*');
        $this->db->from('blogs');
        $this->db->where('blogs.deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog list');
            die();
        }
        else {
            $this->db->select('blogs.*');
            $this->db->from('blogs');
            $this->db->where('blogs.deleted_date', '0000-00-00');
            $this->db->order_by('created_date', 'desc');
            $result = $this->db->get()->num_rows();
            return $result;
        }
    }


    /**
     * Function for getting Bolg list
     * @return mixed
     */
    public function getBlogList()
    {
        $this->db->select('blogs.*');
        $this->db->from('blogs');
        $this->db->where('blogs.deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog list');
            die();
        }
        else {
            $this->db->select('blogs.*');
            $this->db->from('blogs');
            $this->db->where('blogs.deleted_date', '0000-00-00');
            $this->db->order_by('created_date', 'desc');
            $result = $this->db->get()->result();
            return $result;
        }
    }


    /**
     * Function for getting blog info
     * @param $blog_id
     * @return mixed
     */
    public function getBlogInfo($blog_id)
    {
        $this->db->select('*');
        $this->db->from('blogs');
        $this->db->where('blog_id', $blog_id);
        $this->db->where('deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog info');
            die();
        }
        else {
            $this->db->select('*');
            $this->db->from('blogs');
        $this->db->where('blog_id', $blog_id);
            $this->db->where('deleted_date', '0000-00-00');
            $result = $this->db->get()->row();
            return $result;
        }
    }


    /**
     * Function for getting blog info
     * @param $blog_id
     * @return mixed
     */
    public function getBlogTags($blog_id) 
    {
        $this->db->select('blog_tags.* ,tags.*');
        $this->db->from('blog_tags');
        $this->db->join('tags', 'tags.id=blog_tags.tag_id');
        $this->db->where('blog_tags.blog_id', $blog_id);
        $this->db->where('tags.deleted_date','0000-00-00');
        $this->db->where('blog_tags.tag_deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog tag list');
            die();
        }
        else {
            $this->db->select('blog_tags.* ,tags.*');
            $this->db->from('blog_tags');
            $this->db->join('tags', 'tags.id=blog_tags.tag_id');
            $this->db->where('blog_tags.blog_id', $blog_id);
            $this->db->where('tags.deleted_date','0000-00-00');
            $this->db->where('blog_tags.tag_deleted_date', '0000-00-00');
            $result = $this->db->get()->result();
            return $result;
        }
    }


    /**
     * Function for updating users info
     * @param $model_data
     */
    public function updateUserInfo($model_data)
    {
        $admin_id = $model_data['admin_id'];
        $model_data['created_date'] = date('Y-m-d');
    
        if (empty($admin_id)) {          
            $this->db->insert('admin',$model_data);
            return true;
        } else {
            $this->db->where('admin_id',$admin_id);
            $this->db->update('admin',$model_data);
            return true;
        }
    }

    public function updatePassword($model_data)
    {
        $this->db->where('admin_id',$model_data['admin_id']);
        $this->db->update('admin',$model_data);
        return true;

    }

        public function updateDonarInfo($model_data)
    {
        $donar_id = $model_data['id'];
      
        if (empty($donar_id)) {            
            $this->db->insert('donors',$model_data);
        } else {
            $this->db->where('id',$donor_id);
            $this->db->update('donors',$model_data);
        }
        return true;
    }
    /**
     * Function for getting blog info
     * @param $blog_id
     * @param $title
     * @param $description
     * @param $date
     * @param $image
     * @return mixed
     */
    public function updateBlog($blog_id, $title, $description, $img_name, $tags)
    {
        $data = array(
            'title' => $title,
            'description' => $description,
            'img_name' => $img_name,
            'updated_date' => date('Y-m-d H:i:s'),
        );
        $this->db->where('blog_id', $blog_id);
        $this->db->update('blogs', $data);

        
        if(isset($tags) && !empty($tags)) 
        { 

            foreach($tags as $tag) {
                $blog_data = array(
                    'blog_id' => $blog_id,
                    'tag_id' => $tag,
                    'update_date' => date('Y-m-d H:i:s'),
                );

                $status = $this->checkTagVal($blog_data);
                if(!isset($status) && empty($status))
                {
                    $this->db->insert('blog_tags',$blog_data);
                }

            }
        }
        return true;
    }


    public function checkTagVal($blog_data)
    {
        $this->db->select('*');
        $this->db->from('blog_tags');
        $this->db->where('blog_id', $blog_data['blog_id']);
        $this->db->where('tag_id', $blog_data['tag_id']);
        $this->db->where('tag_deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog tag list');
            die();
        }
        else {
            $this->db->select('*');
            $this->db->from('blog_tags');
            $this->db->where('blog_id', $blog_data['blog_id']);
            $this->db->where('tag_id', $blog_data['tag_id']);
            $this->db->where('tag_deleted_date', '0000-00-00');
            $result = $this->db->get()->row();
            return $result;
        }
    }


    public function addBlog($title, $description, $img_name, $tags)
    {
        $data = array(
            'title' => $title,
            'description' => $description,
            // 'date' => $date,
            'created_date' => date('y-m-d H:i:s'),
            'img_name' => $img_name,
            'updated_date' => date('Y-m-d H:i:s'),
            // 'tags' => $tags
        ); 
        $this->db->insert('blogs',$data);
        $blog_id =  $this->db->insert_id();


        if(isset($tags) && !empty($tags)) 
        { 
            foreach($tags as $tag) {
                $blog_data = array(
                    'blog_id' => $blog_id,
                    'tag_id' => $tag,
                    'update_date' => date('Y-m-d H:i:s'),
                );

                $status = $this->checkTagVal($blog_data);
                if(!isset($status) && empty($status))
                {
                    $this->db->insert('blog_tags',$blog_data);
                }

            }
        }
        return $blog_id;
    
    }


    public function updateUserPasswordInfo($model_data)
    {

        $admin_id = $model_data['admin_id'];
       $new_password = $model_data['password'];
    
        $PASSWORD = password_hash($new_password, PASSWORD_BCRYPT);

        $sql = "UPDATE `admin` SET `password` = '$PASSWORD' WHERE admin_id = '$admin_id'";
        $this->db->query($sql);
    }

    public function updateRate($model_data)
    {
        if($this->db->update('rate_table',$model_data)){
            return true;
        }
    }


    public function editPartyAdminProfilePicture($party_id, $image)
    { 
        $data = array(
            'party_image' => $image,
        );
        $this->db->where('party_id', $party_id);
        $this->db->update('user_login', $data);
        return true;
    }

    public function deleteBlog($blog_id)
    {
        $this->deleteBlogAllTag($blog_id);
        $data = array(
            'deleted_date' => date('Y-m-d'),
        );
        $this->db->where('blog_id', $blog_id);
        if (!$this->db->update('blogs',$data)) {
            show_error('Error deleting blog');
            die();
        }
        else{
            return true;
        }
    }

    public function deleteBlogAllTag($blog_id)
    {
        $data = array(
            'tag_deleted_date' => date('Y-m-d'),
        );
        $this->db->where('blog_id', $blog_id);
        if (!$this->db->update('blog_tags', $data)) {
            show_error('Error deleting all tag of blog');
            die();
        }
        else{
            return true;
        }
    }

    public function deleteTag($tag_id)
    {
        $this->db->where('tag_id', $tag_id);
        if (!$this->db->delete('blog_tags')) {
            show_error('Error deleting tag');
            die();
        }
        else{
            return true;
        }
    }

    public function addNewTag($tag_names)
    {   
        $data = array(
            'name'=> strtolower($tag_names),
            'created_date' => date('Y-m-d H:i:s'),
        );
        $status = $this->checkTag($data['name']);
        if(!isset($status) && empty($status))
        {
            $this->db->insert('tags',$data);
        }

        
    }


    public function checkTag($name)
    {
        $this->db->select('*');
        $this->db->from('tags');
        $this->db->like('LOWER(name)', strtolower($name), 'both');
        $this->db->where('deleted_date', '0000-00-00');

        if(!$this->db->get()) {
            show_error('Error in getting blog tag list');
            die();
        }
        else {
            $this->db->select('*');
            $this->db->from('tags');
            $this->db->like('LOWER(name)', strtolower($name), 'both');
            $this->db->where('deleted_date', '0000-00-00');
            $result = $this->db->get()->row();
            return $result;
        }
    }

}