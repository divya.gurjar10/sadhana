-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 28, 2019 at 02:48 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `second_thought`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` int(11) NOT NULL,
  `img_name` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `description` longtext NOT NULL,
  `date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_Date` datetime NOT NULL,
  `deleted_Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`blog_id`, `img_name`, `title`, `description`, `date`, `created_date`, `updated_Date`, `deleted_Date`) VALUES
(27, 'Nature_Bridge.jpg', 'Happy Adventures in The Woods', '<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. \r\nNulla consequat massa quis enim. Donec pede justo, fringilla vel, \r\naliquet nec, vulputate eget, arcu. […] <br></p>', '0000-00-00 00:00:00', '2019-08-28 14:23:33', '2019-08-28 14:23:49', '0000-00-00'),
(28, 'Screenshot_from_2019-08-28_11-46-49.png', 'tags', '<p>tags<br></p>', '0000-00-00 00:00:00', '2019-08-28 14:42:56', '2019-08-28 14:47:07', '2019-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `blog_tag_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(200) NOT NULL,
  `tag_deleted_date` date NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`blog_tag_id`, `blog_id`, `tag_id`, `tag_name`, `tag_deleted_date`, `update_date`) VALUES
(267, 27, 36, '', '0000-00-00', '2019-08-28 14:23:49'),
(268, 28, 36, '', '2019-08-28', '2019-08-28 14:42:56'),
(269, 28, 37, '', '2019-08-28', '2019-08-28 14:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `deleted_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_date`, `deleted_date`) VALUES
(36, 'Tag One', '2019-08-28 14:06:14', '0000-00-00'),
(37, 'tag two', '2019-08-28 14:16:15', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `party_id` int(11) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0:active, 1:deactive',
  `party_image` varchar(200) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `deleted_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`party_id`, `user_type`, `email`, `first_name`, `last_name`, `phone`, `address`, `city`, `postal_code`, `password`, `status`, `party_image`, `created_date`, `deleted_date`) VALUES
(1, 'admin', 'admin@admin.com', 'Admin ', 'Admin', NULL, NULL, NULL, NULL, '$2y$10$mTVAPBeKBgsIGxvzg8o5tOW1swcLoqybPEE.OW0K0abISdN2zx6/e', 0, 'http://localhost/second_thought/admin/images/profile/Nature_Bridge.jpg', '2018-12-15 00:00:00', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`blog_tag_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`party_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `party_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
