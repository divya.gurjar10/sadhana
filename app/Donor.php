<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{

    public function card(){
      $this->belongsTo(Donor::class);
    }
    

}
