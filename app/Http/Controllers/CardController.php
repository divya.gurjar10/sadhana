<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
Use App\Card;
use App\Models\CardModel;
use DB;
class CardController extends Controller
{
  protected $fromAmount = 0;
  protected $toAmount = 0;
  protected $amount = 0;
  protected $activeVal = 0;

    public function index()
    {
      $count = CardController::vacantCardsCount();
      return view('donate.price-card', compact('count'));
    }

    private function vacantCardsCount() //Counting the number of vacant cards in each range
    {
      $index = 0;
      $count = [];
        for($i=1; $i<=5000; $i+=500)
        {
          $count[$index] = DB::table('cards')
                ->whereBetween('card_value', [$i,$i+499])
                ->where('active','=', 1)
                ->count();
                $index++;
        }
        return $count;
    }

    public function generateCards()//funtion to generate cards from Rs 1 to 5000 and save to DB
    {
      for($i=1; $i<=5000; $i++)
      {
      $card = new Card();
      $card->card_value = $i;
      $card->active = 1;
      $card->save();
      }
    }
  public function listCards(Request $request)
    {
      
      $fromAmount = $request->get('range-value-from');
      $toAmount = $request->get('range-value-to');
      $activeVal=$request->get('active');

      $cards = DB::table('cards')
                ->whereBetween('card_value', [$fromAmount, $toAmount])
              
                ->get();

      return view('donate.select-card', compact('cards'));
    }
    

    public function showFillDetails(Request $request)
    {
      $amount = $request->get('amount-selected'); 
      $data['amount'] = $amount;
      // $activeVal = $request->get('active');
      // $data['active']=$active;

      $data['stateList'] = DB::table('geo')
                ->where('geo_type_id', 'STATE')
                ->get();

      $data['countryList'] = DB::table('geo')
                ->where('geo_type_id', 'COUNTRY')
                ->get();

      return view('donate.fill-details', compact('data'));

    }

    
}

