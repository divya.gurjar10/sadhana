<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  protected  $guarded =[];

  public function donor(){
    return $this->hasOne(Card::class);
  }

  

}
