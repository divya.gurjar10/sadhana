<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front-end.index');
});
Route::get('/about', function () {
    return view('front-end.about');
});
// Route::get('//donate/select-card', function () {
//     return view('donate.select-card');
// });
// Route::get('/donate/price-card', function () {
//     return view('donate.price-card');
// });
Route::get('/donate/price-card', 'CardController@index');

Route::post('/donate/select-card', 'CardController@listCards')->name('select-card');
Route::post('/donate/fill-details', 'CardController@showFillDetails')->name('fill-details');
Route::get('/donate/payment', 'DonorController@create')->name('payment');

Route::get('/generate-cards', 'CardController@generateCards');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
?>